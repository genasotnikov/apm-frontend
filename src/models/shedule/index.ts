export type LessonType = {
    id?: number
    name: string
    time: number
};

export type Lesson = {
    id?: number
    content: string
    type: LessonType
    typeId?: number
};

export type LessonInSchedule = {
   lesson: Lesson
   groupId: number
};

export type SubgroupLessonImport = {
    isChisl: boolean,
    dayIndex: number,
    subgroupId: number,
    weekIndex: number,
    lessonId: number
};