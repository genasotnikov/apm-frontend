export interface Faculty {
    id: number
    name: string
    shortness: string
}

export interface Profile {
    id: number
    name: string
    shortness: string
    faculty: Faculty
}

export interface Group {
    id: number,
    profile: Profile,
    number: number
    year: number
}

export interface Subgroup {
    id: number,
    number: number
}

export interface GroupWithSubgroup extends Group {
    maxSubgroups?: number
    subgroups: Array<Subgroup>
}
