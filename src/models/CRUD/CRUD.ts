import {DocumentNode} from "graphql";

export type ColumnMask = {
    label: string;
    key: string;
}

interface CrudOperations {
    createMutation: DocumentNode;
    deleteMutation: DocumentNode;
    countQuery: DocumentNode;
    getPaginatedQuery: DocumentNode;
}

abstract class CRUD<T extends Object> implements CrudOperations {
    abstract countQuery: DocumentNode;
    abstract createMutation: DocumentNode;
    abstract deleteMutation: DocumentNode;
    abstract getPaginatedQuery: DocumentNode;

    protected getDefaultColumnMask: (value: T) => ColumnMask[] = (value) => {
        return Object.keys(value).map(key => ({ key: key, label: key }));
    };

    abstract columnsMask: ColumnMask[];

    abstract get initialValue(): T;
}

export default CRUD;
