import CRUD from "./CRUD";
import {SemesterType} from "../Models";
import {
    getPaginatedSemester as getPaginatedSemesterQuery,
    getCountOfSemester as getCountOfSemesterQuery,
    createSemester as createSemesterMutation,
    deleteSemester as deleteSemesterMutation
} from "./../../api/queries/semester";

class SemesterCRUD extends CRUD<SemesterType> {
    countQuery = getCountOfSemesterQuery;
    createMutation = createSemesterMutation;
    deleteMutation = deleteSemesterMutation;
    getPaginatedQuery = getPaginatedSemesterQuery;
    columnsMask = [
        {label: "id", key: "id"},
        {label:"Год", key: "year"},
        {label: "Первый семестр", key: "isFirstSemester"}];

    get initialValue(): SemesterType {
        return {
            year: (new Date()).getFullYear(),
            isFirstSemester: false
        };
    }
}

export default SemesterCRUD;
