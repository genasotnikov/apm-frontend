import CRUD from "./CRUD";
import {GroupType} from "../Models";
import {
    getPaginatedGroup as getPaginatedGroupQuery,
    getGroupCount as getGroupCountQuery,
    createGroup as createGroupMutation,
    deleteGroup as deleteGroupMutation
} from "./../../api/queries/groups";

class GroupCRUD extends CRUD<GroupType> {
    columnsMask = [
        { label: "id", key: "id", type: "id" },
        { label:"Год", key: "year", type: "year" },
        { label: "Профиль", key: "profileId", type: "foreignKey"},
        { label: "Форма обучения", key: "formOfEdu", type: "string"},
        { label: "Тип образования", key: "typeOfEdu", type: "string"},
    ];
    countQuery =  getGroupCountQuery;
    createMutation = createGroupMutation;
    deleteMutation = deleteGroupMutation;
    getPaginatedQuery = getPaginatedGroupQuery;

    get initialValue(): GroupType {
        return {
            typeOfEdu: "БАКАЛАВРИАТ",
            number: 1,
            year: (new Date()).getFullYear(),
            formOfEdu: "ОЧНАЯ",
            profileId: 0
        };
    }
}

export default GroupCRUD;
