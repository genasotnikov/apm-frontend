import { GroupType } from "../../Models";

interface Subgroup {
    id:number;
    number: number;
    group: GroupType;
}

export default Subgroup;
