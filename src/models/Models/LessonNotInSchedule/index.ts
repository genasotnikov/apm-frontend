import {LessonType, OverrideDayType} from "../../Models";

interface LessonNotInSchedule {

    readonly id: number;

    day: OverrideDayType;

    lesson: LessonType;
}

export default LessonNotInSchedule;
