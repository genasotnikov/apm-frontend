import { LessonType } from "../";

interface Day {
    teacherId: string;

    date: Date;

    lessons: LessonType[];
}
