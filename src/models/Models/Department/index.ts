interface Department {

    readonly name: string;
    readonly director: string;
}

export default Department;
