import {LessonType, ScheduleType} from "../../Models";

interface LessonInSchedule {
    id: number;
    dayIndex: number;
    weekIndex: number;
    isChisl: boolean;
    schedule: ScheduleType;
    lesson: LessonType;
}

export default LessonInSchedule;
