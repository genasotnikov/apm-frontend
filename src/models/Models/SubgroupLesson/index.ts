import {LessonInScheduleType, SubgroupType} from "../../Models";

interface SubgroupLesson {
    id: number;
    subgroup: SubgroupType;
    lessonInSchedule: LessonInScheduleType;
}

export default SubgroupLesson;
