import { TeacherType }  from "../index";

interface User {
    id: number;
    email:string;
    socialToken?:string;
    teacher: TeacherType;
}

export default User;
