import {ProfileType} from "../index";

type formOfEdu = "ОЧНАЯ" | "ЗАОЧНАЯ";
type typeOfEdu = "АСПИРАНТУРА" | "БАКАЛАВРИАТ" | "МАГИСТРАТУРА";

interface GroupInfoPayload {
    profile: ProfileType
    year:number
    groupNumber: string
    formOfEdu: formOfEdu
    typeOfEdu: typeOfEdu
}

interface Group {
    id?: number;
    profile?: ProfileType;
    profileId: number;
    year: number;
    typeOfEdu: typeOfEdu;
    formOfEdu: formOfEdu;
    number: number;
}

export default Group;
