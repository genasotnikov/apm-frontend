interface Semester {
    id?: number;
    year: number;
    isFirstSemester: boolean;
}

export default Semester;
