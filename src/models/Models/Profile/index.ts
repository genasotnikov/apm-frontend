import {FacultyType} from "../../Models";

interface Profile {
    id: number;
    name:string;
    shortness: string;
    facultyId: number;
    faculty: FacultyType;
}

export default Profile;
