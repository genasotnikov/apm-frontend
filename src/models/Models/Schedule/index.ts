interface Schedule {
    id: number;
    semesterId: number;
    teacherId: number;
    isFinished: boolean;
}

export default Schedule;
