import Department from "../Department";

interface Teacher {
    readonly firstName: string;
    readonly middleName: string;
    readonly lastName: string;
    readonly department: Department;
    readonly hasSchedule: boolean;
}

export default Teacher;
