
interface LessonType {
    readonly id: number;
    name: string;
    time: number;
}

export default LessonType;
