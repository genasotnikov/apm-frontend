
interface OverrideDay {

    id: number;
    
    date: Date;
}

export default OverrideDay;
