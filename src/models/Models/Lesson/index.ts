import {LessonTypeType} from "../../Models";



interface Lesson {
    id: number;
    content: string;
    type: LessonTypeType;
}

export default Lesson;
