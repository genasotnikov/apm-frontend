export type hourValue = {
    lessonInScheduleId?: number,
    lessonId?: number,
    groups?: Array<number>
};

export type rowValue = {
    hourSchedules: Array<Array<hourValue>>
};

export type addGroupPayload = {
    newValue: number,
    dayIndex: number,
    chislInd: number
};

export type addLessonPayload = {
    newValue: number,
    dayIndex: number,
    chislInd: number
};

export type removeLessonFromSchedulePayload = {
    dayIndex: number,
    chislInd: number
}

export type dayScheduleState = {
    formData: rowValue
    weekIndex: number
    dayScheduleLoading: boolean
};

export default dayScheduleState;
