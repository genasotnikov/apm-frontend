import UserDataState from "./userData";
import Lessons from "./lessons";
import LessonTypesState from "./lessonTypes";
import DayScheduleState from "./daySchedule";
import VedomostState from "./VedomostState";

export type ReduxStateKeys = "userData" | "lessons" | "lessonTypes" | "daySchedule" | "vedomost";

interface ReduxState {
    userData: UserDataState
    lessons: Lessons
    lessonTypes: LessonTypesState
    daySchedule: DayScheduleState
    vedomost: VedomostState
}

export default ReduxState;
