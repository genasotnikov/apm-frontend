import ErrorLoading from "./errorLoading";
import {LessonType} from "../shedule";

interface LessonTypesState extends ErrorLoading{
    lessonTypes: Array<LessonType>;
}

export default LessonTypesState;