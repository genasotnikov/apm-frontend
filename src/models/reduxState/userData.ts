import ErrorLoading from "./errorLoading";

interface Department {
    name: string,
    director: string
}

export interface Teacher {
    firstName: string,
    middleName?: string,
    lastName: string,
    hasSchedule: boolean
    department: Department
}

interface UserDataState extends ErrorLoading {
    auth: boolean,
    jwt:  String | null,
    teacher: Teacher | null,
}

export default UserDataState;