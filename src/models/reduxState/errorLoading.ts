export default interface ErrorLoading {
    error: string,
    loading: boolean
};