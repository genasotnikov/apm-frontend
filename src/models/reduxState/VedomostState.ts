export type TypeOfLoad = {
    content: string,
    time: number
}

export type LessonData = {
    dayIndex: number,
    faculty: string,
    groupSubgroups: Array<string>,
    lessonType: string,
    content: string,
    year: number,
};

export type DayData = {
    date: Date,
    lessonsData: Array<LessonData>
}

type VedomostState = {
    vedomost: Array<DayData>
    loading: boolean;
    error: boolean;
    typesOfLoad: Array<TypeOfLoad>
}

export default VedomostState;
