import { Lesson } from "../shedule";
import ErrorLoading from "./errorLoading";

interface LessonsState extends ErrorLoading{
    lessons: Array<Lesson>
}

export default LessonsState;