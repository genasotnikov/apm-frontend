export const getFirstObjectKey: (obj: Object) => string = (object) => {
    return Object.keys(object)[0];
};
