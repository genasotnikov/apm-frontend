import React from "react";
import {AppBar, Button, IconButton, Toolbar, Grid} from "@material-ui/core";
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import {useDispatch} from "react-redux";
import { userDataActions } from "../../data/userData";


const TopPanel = (props:any) => {
    const dispatch = useDispatch();
    const logOut = () => dispatch(userDataActions.logOut());
    return (
    <AppBar position="static">
        <Toolbar variant="dense">
            <Grid container direction="row-reverse">
                <IconButton onClick={logOut}>
                    <ExitToAppIcon style={{color: "white"}} />
                </IconButton>
                <Grid item xs={3}>
                    <Button style={{color: "white", marginTop: 5}}>Аккаунт</Button>
                </Grid>
            </Grid>
        </Toolbar>
      </AppBar>
    );
};

export default TopPanel;