import React, { useEffect, useState } from "react";
import { Grid, MenuItem, Select } from "@material-ui/core";
import { useLazyQuery } from "@apollo/client";
import PageContainer from "../../PageContainer";
import { BlueButton } from "../../styled/buttons";
import Vedomost from "../../modals/Vedomost";
import { downloadVedomost as downloadVedomostQuery } from "../../../api/queries/vedomost";
import { useSelector } from "react-redux";
import ReduxState from "../../../models/reduxState";

const months = [
  "Январь",
  "Февраль",
  "Март",
  "Апрель",
  "Май",
  "Июнь",
  "Июль",
  "Август",
  "Сентябрь",
  "Октябрь",
  "Ноябрь",
  "Декабрь",
  "Январь",
];

const Vedomosti = ({}) => {
  const [selectedMonth, setSelectedMonth] = useState(0);
  const [isOpen, setIsOpen] = useState(false);
  const [
    downloadVedomostInString,
    { data, loading, error },
  ] = useLazyQuery<any>(downloadVedomostQuery);
  const jwt = useSelector((state: ReduxState) => state.userData.jwt);

  const uploadFile = () => {
    downloadVedomostInString({
      variables: { year: 2020, monthIndex: selectedMonth },
      context: { headers: { Authorization: jwt } },
    });
  };

  useEffect(() => {
    if (data && (data as { uploadVedomost: string }).uploadVedomost) {
      console.log("uploadVedomost", data?.uploadVedomost);
      window.open("http://localhost:4000/loadVedomost/" + data?.uploadVedomost)
    }
  }, [data]);

  return (
    <PageContainer>
      <Grid container alignItems="center" justify="center">
        <Grid item xs={12} container>
          <Grid item xs={2} />
          <Grid item xs={4}>
            <Select
              labelId="demo-simple-select-placeholder-label-label"
              name="select_month"
              value={selectedMonth}
              onChange={(e: any) => {
                if (typeof e.target.value === "number")
                  setSelectedMonth(e.target.value);
              }}
            >
              {months.map((month, index) => (
                <MenuItem key={index} value={index}>
                  {month}
                </MenuItem>
              ))}
            </Select>
          </Grid>
          <Grid item xs={4} container direction="row" justify="space-around">
            <Grid item>
              <BlueButton
                onClick={() => setIsOpen(true)}
                content={"Посмотреть"}
              />
            </Grid>
            <Grid item>
              <BlueButton onClick={uploadFile} content={"Скачать"} />
            </Grid>
          </Grid>
          <Grid item xs={2} />
        </Grid>
        <Vedomost
          month={selectedMonth}
          year={2020}
          isOpen={isOpen}
          close={() => setIsOpen(false)}
        />
      </Grid>
    </PageContainer>
  );
};

export default Vedomosti;
