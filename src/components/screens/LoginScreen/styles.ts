import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
    image: {
        backgroundImage: 'url(http://www.vspu.ac.ru/a/74/eb/5e7c7384048e7.jpg)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    }
}));