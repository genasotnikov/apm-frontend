import React, { useEffect } from "react";
import { Grid, Container } from "@material-ui/core";
import useStyles from "./styles";
import LoginForm from "../../forms/Login";
import {useSelector} from "react-redux";
import ReduxState from "../../../models/reduxState";
import {useHistory} from "react-router";

const LoginScreen = () => {
    const classes = useStyles();
    const history = useHistory();

    const auth = useSelector((state: ReduxState) => state.userData.auth);
    useEffect(() => {
        if (auth) history.push("/");
    },[auth]);
    return (
        <Container maxWidth={false} disableGutters>
            <Grid container component="main">
                <Grid item xs={false} sm={4} md={7} className={classes.image} />
                <Grid item sm={8} md={4}>
                    <LoginForm />
                </Grid>
            </Grid>
        </Container>
    );
};

export default LoginScreen;