import React, {useState} from "react";
import {DialogTitle, Grid,} from "@material-ui/core";
import MomentUtils from '@date-io/moment';
import PageContainer from "../../PageContainer";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import {MaterialUiPickersDate} from "@material-ui/pickers/typings/date";
import DaySchedule from "../../forms/DaySchedule/index.";
import MyLessons from "../../tables/MyLessons";
import {GreenButton} from "../../styled/buttons";
import {CreateLesson} from "../../modals";
import {useDispatch} from "react-redux";
import dayScheduleActions from "../../../data/daySchedule/actions";
import DatePicker from "../../forms/utils/DatePicker";

const AddWork = ({}) => {
    const [selectedDate, setSelectedDate] = React.useState<string | null | undefined>(null);
    const [lessonModal, setLessonModal] = useState(false);
    const dispatch = useDispatch();

    const handleDateChange = (date: MaterialUiPickersDate | null, value?: string | null | undefined) => {
        setSelectedDate(value);
        //TODO заменить константу semesterId переменной
        dispatch(dayScheduleActions.getDaySchedule((date && date.day() || 0) - 1, 2));
    };
    return (
        <PageContainer>
            <Grid xs={3} />
            <Grid xs={6}>
                <DatePicker value={selectedDate} onChange={handleDateChange}/>
            </Grid>
            <Grid xs={3} />
            <Grid xs={12}>
                {selectedDate && (
                    <>
                        <Grid container>
                            <Grid item xs={3} />
                            <Grid item xs={6}>
                                <MyLessons/>
                            </Grid>
                            <Grid item xs={3} />
                            <Grid item xs={3} />
                            <Grid item xs={6}>
                                <GreenButton content="Добавить урок" onClick={() => setLessonModal(true)} />
                            </Grid>
                            <Grid container item xs={12}>
                                <Grid item xs={4} />
                                <Grid item xs={4}>
                                    <DialogTitle>{`Расписание на ${selectedDate}`}</DialogTitle>
                                </Grid>
                                <Grid item xs={4} />
                            </Grid>
                            <Grid xs={12} style={{marginBottom: "10vh"}}>
                                <DaySchedule edit />
                            </Grid>
                        </Grid>
                    </>
                )}
            </Grid>
            <CreateLesson isOpen={lessonModal} close={() => setLessonModal(false)} />
        </PageContainer>
    )
};

export default AddWork;
