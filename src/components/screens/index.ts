import HomeScreen from "./HomeScreen";
import LoginScreen from "./LoginScreen";
import ScheduleImport from "./Schedule";
import VedomostiImport from "./Vedomosti";
import AddWorkImport from "./AddWork";
import DBMSImport from "./DBMS";

export const Home = HomeScreen;
export const Login = LoginScreen;
export const Schedule = ScheduleImport;
export const Vedomosti = VedomostiImport;
export const AddWork = AddWorkImport;
export const DBMS = DBMSImport;
