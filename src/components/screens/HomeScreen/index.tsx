import React, {useState} from "react";
import PageContainer from "../../PageContainer";
import {Grid} from "@material-ui/core";
import MyLessons from "../../tables/MyLessons";
import {GreenButton} from "../../styled/buttons";
import {CreateLesson} from "../../modals";

const HomeScreen = ({}) => {
    const [lessonModal, setLessonModal] = useState(false);
    return (
        <PageContainer>
            <Grid container direction="row">
                <Grid xs={4} item />
                <Grid xs={4} item>
                    <Grid xs={12} item>
                        <MyLessons />
                    </Grid>
                    <Grid xs={12} item>
                        <GreenButton content="Добавить урок" onClick={() => setLessonModal(true)} />
                    </Grid>
                </Grid>
                <Grid xs={4} item />
            </Grid>
            <CreateLesson isOpen={lessonModal} close={() => setLessonModal(false)} />
        </PageContainer>
    )
};

export default HomeScreen;
