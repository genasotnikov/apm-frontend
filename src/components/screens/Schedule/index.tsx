import React, {useEffect} from "react";
import {useQuery} from "@apollo/client";
import {Grid, Button, DialogTitle, Typography, Theme, CircularProgress, Backdrop} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import PageContainer from "../../PageContainer";
import DaySchedule from "../../forms/DaySchedule/index.";
import dayScheduleActions from "../../../data/daySchedule/actions";
import ReduxState from "../../../models/reduxState";
import dayScheduleState from "../../../models/reduxState/daySchedule";
import {getCurrentSemesterQuery} from "../../../api/queries/semester";
import {Skeleton} from "@material-ui/lab";
import {useStyles} from "./styles";

const daysInAWeek = ["Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];

const Schedule = ({}) => {
    const classes = useStyles();
    const { data, loading } = useQuery(getCurrentSemesterQuery);
    const { weekIndex }: Partial<dayScheduleState> = useSelector((state:ReduxState) => state.daySchedule);
    const { dayScheduleLoading } = useSelector((state: ReduxState) => state.daySchedule);
    const dispatch = useDispatch();

    useEffect(() => {
        //TODO заменить константу переменной и дать пользователю возможность выбрать семестер
        dispatch(dayScheduleActions.getDaySchedule(weekIndex, 1));
    }, [weekIndex]);

    return (
        <PageContainer>
            <Grid container>
                {loading
                    ? <Skeleton animation={"wave"} width={"100%"} />
                    : <strong>
                        Расписание на {data.getCurrentSemester.isFirstSemester ? 1 : 2} семестер &nbsp;
                        {data.getCurrentSemester.year} года
                    </strong>
                }
            </Grid>
            <Grid container className={classes.scheduleContainer}>
                <Backdrop className={classes.backdrop} open={dayScheduleLoading}>
                    <CircularProgress color="primary" />
                </Backdrop>
                <Grid xs={4} item>
                    {weekIndex-1>-1 && <Button onClick={() => dispatch(dayScheduleActions.setWeekIndex(weekIndex-1))}>
                        <DialogTitle>{`< ${daysInAWeek[weekIndex-1]}`}</DialogTitle>
                    </Button>}
                </Grid>
                <Grid xs={4} item>
                    <DialogTitle>{daysInAWeek[weekIndex].toUpperCase()}</DialogTitle>
                </Grid>
                <Grid xs={4} item>
                    {weekIndex+1<6 && <Button onClick={() => dispatch(dayScheduleActions.setWeekIndex(weekIndex+1))}>
                        <DialogTitle>{`${daysInAWeek[weekIndex+1]} >`}</DialogTitle>
                    </Button>}
                </Grid>
                <Grid xs={12} item>
                    <DaySchedule />
                </Grid>
            </Grid>
        </PageContainer>
    )
};

export default Schedule;
