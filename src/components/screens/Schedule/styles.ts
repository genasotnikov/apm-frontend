import {createStyles, makeStyles} from "@material-ui/styles";
import {Theme} from "@material-ui/core";

export const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        backdrop: {
            zIndex: theme.zIndex.drawer + 1,
            opacity: "0.8 !important",
            backgroundColor: "white",
            position: "absolute",
        },
        scheduleContainer: {
            position: "relative",
        },
    }),
);
