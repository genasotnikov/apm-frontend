import React from "react";
import AdminTable from "../../tables/AdminTable";
import PageContainer from "../../PageContainer";
import {Grid} from "@material-ui/core";
import SemesterCRUD from "../../../models/CRUD/Semester";
import GroupCRUD from "../../../models/CRUD/Group";

const GroupsData = () => {
    return (
        <PageContainer>
            <Grid container>
                <Grid item xs={12}>
                    <AdminTable crudObject={new SemesterCRUD()}/>
                </Grid>
                <Grid item xs={12}>
                    <AdminTable crudObject={new GroupCRUD()}/>
                </Grid>
            </Grid>
        </PageContainer>
    );
};

export default GroupsData;
