import React, {FC} from "react";
import DayFragment from "./DayFragment";
import {DayData} from "../../../models/reduxState/VedomostState";

interface DaysFragmentProps {
    days:Array<DayData>
}

const DaysFragment: FC<DaysFragmentProps> = ({days}) => {
    return (
        <>
            {days.map((day: DayData) => <DayFragment key={Math.random()} date={day.date} lessons={day.lessonsData} />)}
        </>
    )
};

export default DaysFragment;
