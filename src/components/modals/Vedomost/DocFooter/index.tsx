import React, {FC} from "react";
import "../headerStyles.scss";

interface DocFooterProps {
    departmentDirector: string
}

const DocFooter:FC<DocFooterProps> = ({departmentDirector}) => {
    return (
        <div className="docHeader" style={{flexDirection: "row", justifyContent: "space-between"}}>
            <div>Заведующий кафедрой:</div>
            <div className="left">{departmentDirector}</div>
        </div>
    );
};

export default DocFooter;