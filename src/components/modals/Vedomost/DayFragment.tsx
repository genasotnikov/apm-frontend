import React, {FC} from "react";
import moment from "moment";
import 'moment/locale/ru';

import Row from "./Row";
import {LessonData} from "../../../models/reduxState/VedomostState";

interface DayFragmentProps {
    lessons: Array<LessonData>
    date: Date
}

const DayFragment: FC<DayFragmentProps> = ({lessons, date}) => {

    return (
        <>
            {lessons.map((lesson,index) => (
                <Row
                    key={Math.random()+1}
                    dayOfWeek={index === 0 && moment(date).format("ddd").toUpperCase()}
                    dayOfMonth={index === 0 && moment(date).format("DD")}
                    dayIndex={lesson.dayIndex}
                    faculty={lesson.faculty}
                    year={lesson.year}
                    groupSubgroups={lesson.groupSubgroups}
                    content={lesson.content}
                    typeOfLoad={lesson.lessonType}
                />
            ))}
        </>
    );
};

export default DayFragment;
