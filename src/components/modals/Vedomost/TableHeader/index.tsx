import React from "react";
import './styles.scss';
import {useSelector} from "react-redux";
import ReduxState from "../../../../models/reduxState";


const TableHeader = (props: any) => {
    const typesOfLoad = useSelector((state: ReduxState) => state.vedomost.typesOfLoad);
    return (
        <thead>
            <tr>
                <th className='empty vedomostHeader' />
                <th className="vedomostHeader">Дата</th>
                <th className="vedomostHeader">Часы занятий от до</th>
                <th className="vedomostHeader">Факультет</th>
                <th className="vedomostHeader">Курс</th>
                <th className="vedomostHeader">Группа</th>
                <th className="vedomostHeader">Содержание занятий</th>
                {typesOfLoad.map((typeOfLoad) => (
                    <th key={Math.random()} className='type-of-load vedomostHeader'>{typeOfLoad.content}</th>
                ))}
                <th className='type-of-load vedomostHeader'>Подпись преподавателя</th>
            </tr>
        </thead>
)};

export default TableHeader;
