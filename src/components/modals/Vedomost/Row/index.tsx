import React from "react";
import { timeRangeOfLessons } from "../constants";
import '../cellsStyles.scss';
import {useSelector} from "react-redux";
import ReduxState from "../../../../models/reduxState";

type TypeOfLoadProps = {
    typeOfLoad: string;
}

const TypeOfLoad = ({typeOfLoad}: TypeOfLoadProps) => {
    const typesOfLoad = useSelector((state: ReduxState) => state.vedomost.typesOfLoad);
    return (
        <>
            {typesOfLoad.map(el =>
                <td key={Math.random()} className='vedomostTd typesOfLoad'>
                    {(el.content === typeOfLoad) ? 2 : ""}
                </td>
            )}
        </>
    );
};

interface RowProps {
    dayOfWeek: string | false;
    groupSubgroups: Array<string>;
    dayOfMonth: string | false;
    dayIndex: number;
    faculty: string;
    year: number;
    content:string;
    typeOfLoad: string;
}

const Row = ({
     dayOfWeek,
     dayOfMonth,
     dayIndex,
     faculty,
     year,
     groupSubgroups,
     content,
     typeOfLoad
}: RowProps) => {
    return (
        <tr>
            <td className='day-of-week'>{dayOfWeek && dayOfWeek.toString()}</td>
            <td className="vedomostTd">{dayOfMonth}</td>
            <td className="vedomostTd">{timeRangeOfLessons[dayIndex]}</td>
            <td className="vedomostTd">{faculty}</td>
            <td className="vedomostTd">{year}</td>
            <td className="vedomostTd">{groupSubgroups}</td>
            <td className="vedomostTd">{content}</td>
            <TypeOfLoad typeOfLoad={typeOfLoad} />
            <td className="vedomostTd" />
        </tr>
    )
};

export default Row;
