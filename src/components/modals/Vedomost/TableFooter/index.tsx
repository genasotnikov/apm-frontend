import React from "react";
import '../cellsStyles.scss';
import {useSelector} from "react-redux";
import ReduxState from "../../../../models/reduxState";


const TableFooter = () => {
        const typesOfLoad = useSelector((state: ReduxState) => state.vedomost.typesOfLoad);
        return (
                <tr>
                        <td />
                        <td />
                        <td />
                        <td />
                        <td />
                        <td />
                        <td><b>Итого</b></td>
                        {typesOfLoad.map((typeOfLoad) => (<td>{typeOfLoad.time}</td>))}
                         <td />
                </tr>
        );
};

export default TableFooter;
