import React, {FC} from "react";
import "../headerStyles.scss";

interface DocHeaderProps {
    faculty: string
    department: string
    month: string
    year: number
    teacherName: string
}

const DocHeader:FC<DocHeaderProps> = ({faculty, department, month, year, teacherName}) => {
   return (
      <div className={"docHeader"}>
          <div className={"docHeaderString"}><b>Федеральное государственное бюджетное образовательное учреждение</b></div><br/>
          <div className={"docHeaderString"}>высшего образования</div><br/>
          <div className={"docHeaderString"}>Воронежский государственный педагогический университет</div><br/><br/>
          <div className={"docHeaderString"}><b>Месячная ведомость</b></div><br/>
          <div className={"docHeaderString"}>учета работы профессорско-преподавательского состава</div><br/>
          <div className={"docHeaderString"}>{`за ${month.toLowerCase()} месяц ${year} г.`}</div><br/>
          <div className={"underlined right"}>{faculty}</div>
          <div className={"underlined right"}>{department}</div>
          <div className={"underlined right"}>{department}</div>
          <div className={"underlined left"}>{teacherName}</div>
      </div>
   );
};

export default DocHeader;