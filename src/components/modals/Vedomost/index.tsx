import React, {FC, useEffect} from "react";
import DaysFragment from "./DaysFragment";
import TableHeader from "./TableHeader";
import TableFooter from "./TableFooter";
import {Card, CircularProgress, Dialog} from "@material-ui/core";
import ModalInterface from "../ModalInterface";
import { RedButton } from "../../styled/buttons";
import DocHeader from "./DocHeader";
import DocFooter from "./DocFooter";
import {useDispatch, useSelector} from "react-redux";
import ReduxState from "../../../models/reduxState";
import { vedomostActions } from "../../../data/vedomost";
import "./styles.scss";

interface VedomostProps extends ModalInterface {
    month: number
    year: number
}

const monthsNames = ["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Сентябрь", "Октябрь", "Ноябрь", "Декабрь", "Январь"];


const Vedomost:FC<VedomostProps> = ({isOpen, close, month, year}) => {
    const { vedomost, loading } = useSelector(((state: ReduxState) => state.vedomost));
    const faculty = "Факультет физико-математический";
    const teacher = useSelector((state: ReduxState) => state.userData.teacher);
    const dispatch = useDispatch();
    const department = (teacher && teacher.department);
    const teacherName = (teacher ? `${teacher.lastName} ${teacher.firstName[0]}. ${teacher.middleName ? teacher.middleName[0]+"." : ""}`: "");

    useEffect(() => {
        dispatch(vedomostActions.getVedomost(month));
    }, [month]);
    if (!department) throw new Error("Отсутствуют данные о кафедре преподавателя!");

    return (
        <Dialog maxWidth={false} fullScreen open={isOpen}>
            <Card style={{ overflow: "scroll"}}>
                {loading ? <CircularProgress />
                : <>
                        <DocHeader teacherName={teacherName} month={monthsNames[month]} year={year} department={department.name} faculty={faculty} />
                        <table className="vedomost">
                            <TableHeader />
                            <tbody>
                                <DaysFragment days={vedomost} />
                                <TableFooter />
                            </tbody>
                        </table>
                        <br/>
                        <DocFooter departmentDirector={department.director} />
                    </>
                }
                <RedButton onClick={close} content={"Закрыть"}/>
            </Card>
        </Dialog>
)};

export default Vedomost;
