import React, { FC } from "react";
import { Formik, Form, Field, } from "formik";
import { Dialog, Grid, Card, DialogTitle, MenuItem } from "@material-ui/core";
import { TextField, Select } from 'formik-material-ui';
import {useDispatch} from "react-redux";
import { LessonType } from "../../../models/shedule";
import {GreenButton, RedButton} from "../../styled/buttons";
import ModalInterface from "../ModalInterface";
import {lessonTypeActions} from "../../../data/lessonTypes"

const timeValues = [0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2];

const CreateLessonType: FC<ModalInterface> = ({isOpen, close}) => {
    const initialValues:LessonType = { name: "", time: 2 };
    const dispatch = useDispatch();

    const submit = ({...values}: LessonType) => {
        dispatch(lessonTypeActions.createLessonType({ name: values.name, time: values.time }));
        close();
    };

    return(
          <Dialog open={isOpen}>
              <Card>
                  <Formik initialValues={initialValues} onSubmit={submit}>
                      {({values, handleSubmit, isSubmitting}) => (
                          <Form>
                              <DialogTitle>
                                  Новая нагрузка
                              </DialogTitle>
                              <Grid xs={12}>
                                  <Field
                                      value={values.name}
                                      component={TextField}
                                      name="name"
                                      type="text"
                                      placeholder="Название нагрузки(это может быть название пары)"
                                      autoFocus
                                  />
                              </Grid>
                              <Grid xs={12} alignItems="center">
                                  <Field value={values.time} component={Select} name="time" type="select" placeholder="Колличество часов">
                                      {timeValues.map(((value) => <MenuItem key={value} value={value}>{value}</MenuItem>))}
                                  </Field>
                              </Grid>
                              <Grid xs={12} container direction="row" justify="space-between">
                                  <Grid xs={5}>
                                      <RedButton onClick={close} content={"Закрыть"}/>
                                  </Grid>
                                  <Grid xs={5}>
                                      <GreenButton disabled={isSubmitting} onClick={handleSubmit} content={"Создать"}/>
                                  </Grid>
                              </Grid>
                          </Form>
                      )}
                  </Formik>
              </Card>
          </Dialog>
)};

export default CreateLessonType;