export default interface SelectGroupProps {
    isOpen: boolean
    close: () => void
};
