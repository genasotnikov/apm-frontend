import CreateLessonTypeImport from "./LessonType";
import LessonImport from "./Lesson";


export const CreateLessonType = CreateLessonTypeImport;
export const CreateLesson = LessonImport;