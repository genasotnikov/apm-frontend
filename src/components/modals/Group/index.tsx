import React, {FC} from "react";
import {Dialog, Card} from "@material-ui/core";
import ModalInterface from "../ModalInterface";
import SubgroupForm from "../../forms/Subgroup";

interface SelectGroupProps extends ModalInterface {
    parentIndex: number
    index: number
}

const SelectGroup: FC<SelectGroupProps> = ({parentIndex, index, isOpen, close}) => {
    return(
        <Dialog open={isOpen}>
            <Card>
                <SubgroupForm parentIndex={parentIndex} index={index} closeModal={close} />
            </Card>
        </Dialog>
    )};

export default SelectGroup;