import React, {FC, useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Dialog, Card, CircularProgress} from "@material-ui/core";
import LessonForm from "../../forms/Lesson";

import ModalInterface from "../ModalInterface";
import ReduxState from "../../../models/reduxState";
import { lessonTypeActions } from "../../../data/lessonTypes";


const CreateLesson: FC<ModalInterface> = ({isOpen, close}) => {
    const { loading, lessonTypes } = useSelector((state: ReduxState) => state.lessonTypes);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(lessonTypeActions.getLessonTypes());
    }, []);

    return (
        <Dialog open={isOpen}>
            <Card>
                {loading ? <CircularProgress />
                : <LessonForm close={close} lessonTypes={lessonTypes} />}
            </Card>
        </Dialog>
    )};

export default CreateLesson;