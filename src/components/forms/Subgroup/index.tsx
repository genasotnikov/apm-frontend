import {Field, Form, Formik} from "formik";
import {DialogTitle, Grid} from "@material-ui/core";
import {TextField} from "formik-material-ui";
import {GreenButton, RedButton} from "../../styled/buttons";
import React from "react";
import {getFaculties} from "../../../api/queries/faculties";
import {QueriedSelect} from "../utils";
import {getGroupNumberViaYearAndProfile, getProfilesOfFaculty, getSubgroupsViaGroupId} from "../../../api/queries";
import {useDispatch} from "react-redux";
import dayScheduleActions from "../../../data/daySchedule/actions";

type formValues = {
    facultyId: number,
    profileId: number,
    year: number,
    groupId: number,
    subGroupId: number
};

const Subgroup = ({closeModal, index, parentIndex}: any) => {
    const initialValues:formValues | {} = {};
    const dispatch = useDispatch();
    const onSubmit = ((values: any) => {
        dispatch(dayScheduleActions.addGroupToHourSchedule({
            newValue: Number(values.subGroupId),
            chislInd: index,
            dayIndex: parentIndex
        }));
        closeModal();
    });
    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit}>
            {({values, handleSubmit}: any) =>
                <Form onSubmit={handleSubmit}>
                    <DialogTitle>
                        Добавление группы
                    </DialogTitle>
                    <Grid xs={12}>Группа:</Grid>
                    <Grid xs={12}>
                        <QueriedSelect
                            name={"facultyId"}
                            placeholder={"факультет"}
                            queryName={"getFaculties"}
                            query={getFaculties}
                            optionTextKey={"shortness"}
                            autoFocus
                            value={values.facultyId}
                        />
                    </Grid>
                    {values.facultyId && (
                        <Grid xs={12}>
                            <QueriedSelect
                                name={"profileId"}
                                placeholder={"профиль"}
                                queryName={"getProfilesOfFaculty"}
                                query={getProfilesOfFaculty}
                                optionTextKey={"name"}
                                variables={{facultyId: Number(values.facultyId)}}
                                value={values.profileId}
                            />
                        </Grid>
                    )}
                    {values.profileId && (
                        <Grid xs={12}>
                            <Field
                                component={TextField}
                                name="year"
                                type="text"
                                placeholder="курс"
                            />
                        </Grid>
                    )}
                    {values.year && (
                        <Grid xs={12}>
                            <QueriedSelect
                                name={"groupId"}
                                placeholder={"номер группы"}
                                queryName={"getGroups"}
                                query={getGroupNumberViaYearAndProfile}
                                optionTextKey={"number"}
                                variables={{profileId: Number(values.profileId), year: (new Date()).getFullYear() - values.year}}
                                value={values.groupId}
                            />
                        </Grid>
                    )}
                    {values.groupId && (
                        <Grid xs={12}>
                            <QueriedSelect
                                name={"subGroupId"}
                                placeholder={"номер подгруппы"}
                                queryName={"getSubgroups"}
                                query={getSubgroupsViaGroupId}
                                optionTextKey={"number"}
                                variables={{groupId: Number(values.groupId)}}
                                value={values.subGroupId}
                            />
                        </Grid>
                    )}
                    <Grid xs={12} container direction="row" justify="space-between">
                        <Grid xs={5}>
                            <RedButton onClick={closeModal} content={"Закрыть"}/>
                        </Grid>
                        <Grid xs={5}>
                            <GreenButton disabled={!!values.subGroupId} type="submit" onClick={handleSubmit} content={"Создать"}/>
                        </Grid>
                    </Grid>
                </Form>
            }
        </Formik>)
};

export default Subgroup;
