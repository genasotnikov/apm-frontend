import React, {FC} from "react";
import MomentUtils from "@date-io/moment";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import {MaterialUiPickersDate} from "@material-ui/pickers/typings/date";

export type DatePickerProps = {
    value: string | undefined | null;
    onChange: (date: MaterialUiPickersDate | null, value?: string | null | undefined) => void;
};

const DatePicker: FC<DatePickerProps> = ({ value, onChange }) => {
    return (
        <MuiPickersUtilsProvider utils={MomentUtils}>
            <KeyboardDatePicker
                okLabel="Выбрать"
                cancelLabel="Закрыть"
                margin="normal"
                id="date-picker-dialog"
                label="Выберите дату"
                format="DD/MM/YYYY"
                value={value}
                onChange={onChange}
                KeyboardButtonProps={{
                    'aria-label': 'change date',
                }}
            />
        </MuiPickersUtilsProvider>
    );
};

export default DatePicker;
