import {Select} from "formik-material-ui";
import {CircularProgress, MenuItem} from "@material-ui/core";
import {Field} from "formik";
import React, {FC} from "react";
import {useQuery} from "@apollo/client";
import {DocumentNode} from "graphql";

interface QueriedSelectProps {
    name: string
    placeholder: string
    queryName: string
    query: DocumentNode
    optionTextKey: string
    autoFocus?: boolean
    value: number | string
    variables?: Object
}

const QueriedSelect: FC<QueriedSelectProps> = ({name, placeholder, queryName, optionTextKey, autoFocus, query, value, variables}) => {
    const { data, loading, error } = useQuery(query, { variables: variables });

    if (loading) return <CircularProgress />;

    if (error) alert(error);

    return (
        <Field value={value} name={name} component={Select} type="select" placeholder={placeholder} autoFocus={autoFocus}>
            {(data[queryName] as Array<any>).map(((value, index) => <MenuItem key={index} value={value.id}>{value[optionTextKey]}</MenuItem>))}
        </Field>
    );
};

export default QueriedSelect;