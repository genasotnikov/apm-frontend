import React, {ChangeEvent, FC} from "react";

import {Field, FieldConfig} from "formik";
import { TextField, Checkbox,  } from 'formik-material-ui';
import {Grid} from "@material-ui/core";

interface FieldByTypeProps extends FieldConfig {
    setFieldValue: (key:string, value: unknown) => void;
}

const FieldByType: FC<FieldByTypeProps> = ({value, setFieldValue, ...props}) => {
    switch (typeof value) {
        case "boolean":
            return (
                <Grid container alignItems={"flex-start"}>
                    <Field value={value} component={Checkbox} {...props} />
                </Grid>
                );
        case "number":
            return (
                <input
                    value={value}
                    name={props.name}
                    type={"number"}
                    onChange={(e: ChangeEvent<HTMLInputElement>) => {
                        e.preventDefault();
                        console.log("e.target.value=", e.target.value);
                        setFieldValue(props.name, Number(e.target.value));
                    }}
                />
            );

        case "string":
            return <Field value={value} component={TextField} {...props} />;

        case "undefined":
            return <div>Ошибка! Не верный тип</div>;
        default: {
            // TODO добавить DatePicker если Data
            return <div>123</div>
        }
    }
};

export default FieldByType;
