import React, {FC} from "react";
import {Form} from "formik";
import {DialogTitle, FormLabel, Grid} from "@material-ui/core";
import FieldByType from "../utils/FieldByType";
import {GreenButton, RedButton} from "../../styled/buttons";
import {ColumnMask} from "../../../models/CRUD/CRUD";

type AddDataToAdminTableFormProps = {
    values: Object;
    columnsMask: ColumnMask[];
    isSubmitting: boolean;
    setFieldValue: (key:string, value: unknown) => void;
    //временное решение
    close: () => void;
    errors: any,
}

const AddDataToAdminTableForm: FC<AddDataToAdminTableFormProps> = ({ values, columnsMask, errors, isSubmitting, setFieldValue, close }) => {
    console.log("values=", values);

    return (
        <Form>
            <DialogTitle>
                Новое занятие
            </DialogTitle>
            <Grid container direction={"column"}>
                {Object.keys(values).map(key => (
                    // TODO убрать any
                    <>
                        <FormLabel>{(columnsMask.filter((mask) => mask.key === key))[0].label}</FormLabel>
                        <FieldByType setFieldValue={setFieldValue} name={key} value={(values as any)[key]} />
                        {errors[key]}
                    </>
                ))}
            </Grid>
            <Grid xs={12} container direction="row" justify="space-between">
                <Grid xs={5}>
                    <RedButton onClick={close} content={"Закрыть"}/>
                </Grid>
                <Grid xs={5}>
                    <GreenButton type={"submit"} disabled={isSubmitting} content={"Создать"}/>
                </Grid>
            </Grid>
        </Form>
    )
};

export default AddDataToAdminTableForm;
