import React, {FC, } from "react";
import { Formik, } from "formik";
import {Card, Dialog} from "@material-ui/core";
import AddDataToAdminTableForm from "../AddDataToAdminTableForm";
import {ColumnMask} from "../../../models/CRUD/CRUD";

export type AddDataToAdminTableProps = {
    initialValues: Object;
    opened: boolean;
    close: () => void;
    // TODO избавиться от any
    submit: (values: any) => Promise<void>;
    columnsMask: ColumnMask[];
};

const AddDataToAdminTable:FC<AddDataToAdminTableProps> = ({initialValues, opened, close, columnsMask, submit}) => {
    return (
        <Dialog open={opened}>
            <Card>
                <Formik initialValues={initialValues} onSubmit={submit}>
                    {({values, isSubmitting, errors, setFieldValue}) => (
                        <AddDataToAdminTableForm
                            setFieldValue={setFieldValue}
                            errors={errors}
                            values={values}
                            columnsMask={columnsMask}
                            isSubmitting={isSubmitting}
                            close={close}
                        />
                    )}
                </Formik>
            </Card>
        </Dialog>
    );
};

export default AddDataToAdminTable;
