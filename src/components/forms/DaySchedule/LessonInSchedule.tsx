import React, {FC} from "react";
import {Grid, IconButton, Tooltip} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import ReduxState from "../../../models/reduxState";
import SelectLesson from "./SelectLesson";
import SubgroupsController from "./SubgroupsController";
import StyledGridCell from "../../styled/StyledGridCell";
import {Delete} from "@material-ui/icons";
import daySheduleActions from "../../../data/daySchedule/actions";


interface LessonScheduleProps {
    parentIndex: number,
    index: number
}

const LessonSchedule: FC<LessonScheduleProps> = ({parentIndex, index}) => {
    const { formData } = useSelector((state:ReduxState) => state.daySchedule);
    const dispatch = useDispatch();

    return (
        <>
            <Grid container>
                <StyledGridCell
                    dayIndex={parentIndex}
                    isChisl={!index}
                    container
                    item
                    xs={5}
                    alignItems="center"
                >
                    <SelectLesson
                        index={index}
                        dayIndex={parentIndex}
                        value={formData.hourSchedules[parentIndex][index].lessonId}
                    />
                </StyledGridCell>
                <StyledGridCell item isChisl={!index} xs={6} dayIndex={parentIndex}>
                    <SubgroupsController parentIndex={parentIndex} index={index}/>
                </StyledGridCell>
                <Grid item xs={1}>
                    <Tooltip title={"Удалить занятие"}>
                        <IconButton onClick={() => dispatch(daySheduleActions.deleteLessonInSchedule(parentIndex, index))}>
                            <Delete />
                        </IconButton>
                    </Tooltip>
                </Grid>
            </Grid>
        </>

    );
};
export default LessonSchedule
