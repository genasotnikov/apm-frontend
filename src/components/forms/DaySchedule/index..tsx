import React, {FC, useEffect, useState} from "react";
import HourSchedule from "./HourSchedule";
import {FieldArray, Form, Formik} from "formik";
import {useDispatch, useSelector} from "react-redux";
import {Grid, Paper} from "@material-ui/core";
import {WhiteButton} from "../../styled/buttons";
import {CreateLesson} from "../../modals";
import {rowValue} from "../../../models/reduxState/daySchedule";
import ReduxState from "../../../models/reduxState";
import dayScheduleActions from "../../../data/daySchedule/actions";


interface DayScheduleProps {
    edit?: boolean
}

const DaySchedule:FC<DayScheduleProps> = ({edit}) => {
    const [lessonModal, setLessonModal] = useState(false);
    const values: rowValue = useSelector((state: ReduxState) => state.daySchedule.formData);
    const initialValues: rowValue = values;
    const dayIndex:Array<number> = Array.from({length: 6});
    const dispatch = useDispatch();

    return (
        <>
            <Formik initialValues={initialValues} onSubmit={() => console.log("Успех")}>
                <Form>
                    <FieldArray
                        name={"hourSchedules"}
                        render={() => {
                            return dayIndex.map((empty, dayIndex) => <HourSchedule key={Math.random()} dayIndex={dayIndex} />)}
                        }
                    />
                </Form>
            </Formik>
            <Grid xs={12} style={{position: "fixed", bottom: "0vh", right: "0vh" }} item>
                <Paper style={{width: "100vw", minHeight: "5vh", marginRight: "20vh"}}>
                    <Grid container direction="row-reverse" style={{paddingRight: "4vh"}}>
                        <Grid>
                            <WhiteButton onClick={() => dispatch(dayScheduleActions.sendDaySchedule())} content="Сохранить расписание на день" />
                        </Grid>
                        {!edit && <Grid>
                            <WhiteButton onClick={() => console.log("Успех")} content="Сохранить расписание на неделю" />
                        </Grid>}
                    </Grid>
                </Paper>
            </Grid>
            <CreateLesson isOpen={lessonModal} close={() => setLessonModal(false)} />
        </>
    );
};

export default DaySchedule;
