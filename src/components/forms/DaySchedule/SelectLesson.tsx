import {useField} from "formik";
import {useDispatch, useSelector} from "react-redux";
import ReduxState from "../../../models/reduxState";
import {MenuItem, Select} from "@material-ui/core";
import dayScheduleActions from "../../../data/daySchedule/actions";
import React, {FC} from "react";

type SelectLessonProps = {
    index: number;
    dayIndex: number;
    value: number | undefined;
}

const SelectLesson:FC<SelectLessonProps> = ({index, value, dayIndex, ...props}) => {
    const [field] = useField(props as any);
    const lessons = useSelector((state:ReduxState) => state.lessons.lessons);
    const dispatch = useDispatch();
    return (
        <Select
            {...field}
            labelId="demo-simple-select-placeholder-label-label"
            name="select_month"
            value={value || false}
            onChange={(e: any) => dispatch(dayScheduleActions.addLessonToHourSchedule({
                chislInd: index,
                newValue: e.target.value,
                dayIndex: dayIndex
            }))}>
            {lessons.map(((value) =>
                <MenuItem key={Math.random()} value={value.id}>
                    {`${value.content} (${value.type.name.slice(0,3).toLowerCase()})`}
                </MenuItem>)
            )}
            <MenuItem key={Math.random()} value={undefined}>
                (оставить пустым)
            </MenuItem>
        </Select>
    );
};

export default SelectLesson;
