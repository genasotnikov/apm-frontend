import React, {FC} from "react";
import LessonSchedule from "./LessonInSchedule";
import { Grid } from "@material-ui/core";
import {FieldArray} from "formik";
import StyledGridCell from "../../styled/StyledGridCell";

interface HourScheduleProps {
    dayIndex: number
}

const lessonTime = ["9:00", "10:45", "12:40", "14:40", "16:25", "18:10"];

const HourSchedule: FC<HourScheduleProps> = ({dayIndex}) => {
    return (
       <Grid container>
           <StyledGridCell style={{borderLeftWidth: 1}} isChisl={false} xs={2} dayIndex={dayIndex} item>{lessonTime[dayIndex]}</StyledGridCell>
           <Grid xs={2} container item>
               <StyledGridCell xs={12} isChisl={true} dayIndex={dayIndex} item>Числитель</StyledGridCell>
               <StyledGridCell xs={12} isChisl={false} dayIndex={dayIndex} item>Знаменатель</StyledGridCell>
           </Grid>
           <Grid xs={8} container item>
               <FieldArray name={"chislAndZnam"} render={() => (
                   <>
                       <Grid xs={12} item>
                           <LessonSchedule index={0} parentIndex={dayIndex} />
                       </Grid>
                       <Grid xs={12} item>
                           <LessonSchedule index={1} parentIndex={dayIndex} />
                       </Grid>
                   </>)}
               />
           </Grid>
       </Grid>
    );
};

export default HourSchedule;
