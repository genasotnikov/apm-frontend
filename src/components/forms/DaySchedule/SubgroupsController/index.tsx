import React, {FC, useState} from "react";
import { SubgroupType } from "../../../../models/Models";
import {useQuery} from "@apollo/client";
import {getSubgroupsByIds} from "../../../../api/queries/subgroups";
import {useSelector} from "react-redux";
import ReduxState from "../../../../models/reduxState";
import {Skeleton} from "@material-ui/lab";
import SubgroupController from "./SubgroupController";
import {Grid, IconButton} from "@material-ui/core";
import SelectGroup from "../../../modals/Group";
import {AddButton} from "../../../styled/buttons";

type SubgroupControllerProps = {
    parentIndex: number;
    index: number;
};

const SubgroupsController: FC<SubgroupControllerProps> = ({parentIndex, index}) => {
    const [groupModal, setGroupModal] = useState(false);
    const { formData } = useSelector((state:ReduxState) => state.daySchedule);
    const groups = formData.hourSchedules[parentIndex][index].groups;

    const { data, loading } = useQuery(getSubgroupsByIds, {
        variables: { ids: groups},
        skip: !(groups && Array.isArray(groups) && groups.length)
    });
    return (
        <>
            {loading
                ? <Skeleton animation={"wave"} />
                : (
                    <Grid container style={{height: "100%"}}>
                        <Grid container direction={"column"} justify={"space-around"} xs={2} style={{height: "100%"}} item>Группы:</Grid>
                        <Grid container alignItems={"center"} xs={9} item>
                            {(data && formData.hourSchedules[parentIndex][index].groups)
                                ? data.getSubgroupsByIds.map((subgroup: SubgroupType) => <SubgroupController subgroup={subgroup} /> )
                                : ""}
                        </Grid>
                        <AddButton content={"Добавить группу"} onClick={() => setGroupModal(true)} />
                        <SelectGroup
                            parentIndex={parentIndex}
                            index={index}
                            isOpen={groupModal}
                            close={() => setGroupModal(false)}
                        />
                    </Grid>
                )
            }

        </>
    );
};

export default SubgroupsController;
