import React, {FC} from "react";
import { Box } from "@material-ui/core";
import {SubgroupType} from "../../../../models/Models";
import {purple, green, red, pink, deepPurple, lightBlue, teal, deepOrange, blueGrey} from '@material-ui/core/colors';

const colors = [purple, green, red, pink, deepPurple, lightBlue, teal, deepOrange, blueGrey, teal];

const getRandomColor = () => {
    const randomIndex = Math.trunc(Math.random() * 10);
    return colors[randomIndex][500];
};

type SubgroupControllerProps = {
    subgroup: SubgroupType;
}

const SubgroupController:FC<SubgroupControllerProps> = ({subgroup}) => {
    return (
        <Box
            bgcolor={getRandomColor()}
            color={"white"}
            padding={"1px 3px 1px 3px"}
            margin={"5px"}
            borderRadius={"10%"}
            component={"span"}>
            {`${(new Date()).getFullYear() - Number(subgroup.group.year)}${subgroup.group?.profile && subgroup.group.profile.shortness}(${subgroup.number})`}
        </Box>
    );
};

export default SubgroupController;
