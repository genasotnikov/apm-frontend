import React from "react";
import { Formik, Form, Field } from "formik";
import { TextField } from 'formik-material-ui';
import { Button, Grid } from '@material-ui/core';
import {useDispatch, useSelector} from "react-redux";
import { userDataActions } from "../../../data/userData";
import ReduxState from "../../../models/reduxState";


const LoginForm = () => {
    const loading = useSelector((state: ReduxState) => state.userData.loading);
    const dispatch = useDispatch();
    return (
        <Grid container justify="space-around">
            <Grid item xs={12}>
                <h1>Ведомости онлайн</h1>
            </Grid>
            <Grid item xs={12}>
                <Formik
                    initialValues={{
                        email: '',
                        password: '',
                    }}
                    onSubmit={(values:Object) => {dispatch(userDataActions.login(values))}}
                >
                    {({submitForm}) => (
                        <Form>
                            <Grid container>
                                <Grid item xs={12}>
                                    <Field disabled={loading} component={TextField} name="email" type="email" placeholder='почта' autoFocus />
                                </Grid>
                                <Grid item xs={12}>
                                    <Field disabled={loading} component={TextField} name="password" type="password" placeholder='пароль' />
                                </Grid>
                                <Grid item xs={12}>
                                    <Button variant="contained" disabled={loading} onClick={submitForm}>
                                        Войти
                                    </Button>
                                </Grid>
                            </Grid>
                        </Form>
                    )}
                </Formik>
            </Grid>
        </Grid>
)};

export default LoginForm;