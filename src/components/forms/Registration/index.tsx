import React from "react";
import { Formik, Form, Field } from "formik";
import { TextField } from 'formik-material-ui';
import * as Yup from "yup";
import { Button, Grid } from '@material-ui/core';

const RegisterSchema = Yup.object().shape({
    email: Yup.string()
        .email('Почта введена некорректно')
        .required('Обязательно'),
    firstName: Yup.string()
        .required('Обязательно'),
    lastName: Yup.string()
        .required('Обязательно'),
    middleName: Yup.string(),
    password: Yup.string()
        .required('Обязательно').min(8, "Длинна пароля должна быть минимум 8 символов"),
    passwordRepeat: Yup.string().oneOf([Yup.ref('password'), null], 'Пароли должны совпадать'),
});

const Registration = () => (
    <Grid container>
        <Grid item xs={12}>
            <h1>Регистрация преподавателя</h1>
        </Grid>
        <Grid item xs={12}>
            <Formik
                initialValues={{
                    firstName: '',
                    lastName: '',
                    middleName: '',
                    email: '',
                    password: '',
                    passwordRepeat: ''
                }}
                validationSchema={RegisterSchema}
                onSubmit={(values, actions) => {
                    setTimeout(() => {
                        alert(JSON.stringify(values, null, 2));
                        actions.setSubmitting(false);
                    }, 1000);
                }}
            >
                {({submitForm, isSubmitting}) => (
                    <Form>
                        <Grid container>
                            <Grid item xs={12}>
                                <Field component={TextField} name="email" type="email" placeholder='почта' autoFocus />
                            </Grid>
                            <Grid item xs={12}>
                                <Field component={TextField} name="password" type="password" placeholder='пароль' />
                            </Grid>
                            <Grid item xs={12}>
                                <Field component={TextField} name="passwordRepeat" type="password" placeholder='подтвердите пароль' />
                            </Grid>
                            <Grid item xs={12}>
                                <Field component={TextField} name="firstName" placeholder='Ваше имя' />
                            </Grid>
                            <Grid item xs={12}>
                                <Field component={TextField} name="middleName" placeholder='Ваше отчество' />
                            </Grid>
                            <Grid item xs={12}>
                                <Field component={TextField} name="lastName" placeholder='Ваша фамилия' />
                            </Grid>
                            <Grid item xs={12}>
                                <Button variant="contained" disabled={isSubmitting} onClick={submitForm}>
                                    Регистрация
                                </Button>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
        </Grid>
    </Grid>
);

export default Registration;