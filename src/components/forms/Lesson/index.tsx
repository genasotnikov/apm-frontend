import {Field, Form, Formik} from "formik";
import {DialogTitle, FormLabel, Grid, MenuItem} from "@material-ui/core";
import {Select, TextField} from "formik-material-ui";
import {GreenButton, RedButton} from "../../styled/buttons";
import React, {FC, useState} from "react";
import {LessonType, Lesson as TypeOfLesson} from "../../../models/shedule";
import CreateLessonType from "../../modals/LessonType";
import {useDispatch} from "react-redux";
import {lessonsActions} from "../../../data/lessons";

interface LessonProps {
    lessonTypes: Array<LessonType>
    close: () => void;
}

const Lesson: FC<LessonProps> = ({lessonTypes, close}) => {
    const [lessonTypeModal, setLessonTypeModal] = useState(false);
    const initialValues:TypeOfLesson = {
        content: "",
        type: lessonTypes[0],
        typeId: lessonTypes[0].id
            ? parseInt(lessonTypes[0].id.toString(), lessonTypes[0].id)
            : lessonTypes[0].id };

    const dispatch = useDispatch();

    const submit = ({...values}: TypeOfLesson) => {
        dispatch(lessonsActions.createLesson({ content: values.content, typeId: Number(values.typeId) }));
        close();
    };

    return (
        <>
            <Formik initialValues={initialValues} onSubmit={submit}>
                {({values, handleSubmit, isSubmitting}) => (
                    <Form>
                        <DialogTitle>
                            Новое занятие
                        </DialogTitle>
                        <Grid xs={12} alignItems="center">
                            <FormLabel>Выберите тип нагрузки</FormLabel>
                            <Field value={values.typeId} component={Select} name="typeId" type="select" placeholder="Тип занятия">
                                {lessonTypes.map(((value, index) => <MenuItem key={index} value={value.id}>{value.name}</MenuItem>))}
                            </Field>
                        </Grid>
                        <GreenButton onClick={() => setLessonTypeModal(true)} content={"Добавить новый тип нагрузки"} />
                        <Grid xs={12}>
                            <FormLabel>Наименование нагрузки (например, название пары)</FormLabel>
                            <Field
                                value={values.content}
                                component={TextField}
                                name="content"
                                type="text"
                                placeholder="Название нагрузки"
                                autoFocus
                            />
                        </Grid>
                        <Grid xs={12} container direction="row" justify="space-between">
                            <Grid xs={5}>
                                <RedButton onClick={close} content={"Закрыть"}/>
                            </Grid>
                            <Grid xs={5}>
                                <GreenButton disabled={isSubmitting} onClick={handleSubmit} content={"Создать"}/>
                            </Grid>
                        </Grid>
                    </Form>
                )}
            </Formik>
            <CreateLessonType isOpen={lessonTypeModal} close={() => setLessonTypeModal(false)} />
        </>
    );
};

export default Lesson;