import React from "react";
import {ListItem, ListItemText, Drawer} from "@material-ui/core";
import {useHistory} from "react-router";
import {useSelector} from "react-redux";
import ReduxState from "../../models/reduxState";

const NavigationMenu = () => {
    const history = useHistory();
    const hasSchedule = true; //useSelector((state: ReduxState) => state.userData.teacher ? state.userData.teacher.hasSchedule : null);
    return (
        <Drawer variant="permanent" anchor="left">
            {hasSchedule ? (
                <>
                    <ListItem button>
                        <ListItemText onClick={() => history.push("/")} primary="Мои занятия" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText onClick={() => history.push("/addWork")} primary="Добавить нагрузку" />
                    </ListItem>
                    <ListItem onClick={() => history.push("/schedule")} button>
                        <ListItemText primary="Редактировать расписание" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText onClick={() => history.push("/vedomosti")} primary="Мои ведомости" />
                    </ListItem>
                    <ListItem button>
                        <ListItemText onClick={() => history.push("/DBMS")} primary="СУБД" />
                    </ListItem>
                </> ) : (
                <>
                    <ListItem button>
                        <ListItemText primary="Добавить тип нагрузки" />
                    </ListItem>
                    <ListItem onClick={() => history.push("/schedule")} button>
                        <ListItemText primary="Создать расписание" />
                    </ListItem>
                </>
            )}
        </Drawer>
)};

export default NavigationMenu;
