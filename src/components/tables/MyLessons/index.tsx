import React, {FC, useEffect} from "react";
import {
    Table,
    TableHead,
    TableBody,
    TableRow,
    TableCell,
    Paper,
    TableContainer,
    CircularProgress
} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import ReduxState from "../../../models/reduxState";
import {Lesson} from "../../../models/shedule";
import { lessonsActions } from "../../../data/lessons";

const MyLessons = () => {
    const lessons: Array<Lesson> = useSelector((state: ReduxState) => state.lessons.lessons);
    const loading: boolean = useSelector((state: ReduxState) => state.lessons.loading);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(lessonsActions.getLessons());
    }, []);


    return (
        <Paper style={{backgroundColor: "unset"}}>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Мои уроки</TableCell>
                            <TableCell>Нагрузка</TableCell>
                        </TableRow>
                    </TableHead>
                    {loading
                        ? <CircularProgress />
                        : <TableBody>
                            {lessons.map((lesson) => (
                                <TableRow key={Math.random()}>
                                    <TableCell>
                                        {`${lesson.content} (${lesson.type.name.slice(0,3).toLowerCase()})`}
                                    </TableCell>
                                    <TableCell>
                                        {lesson.type.time}
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    }
                </Table>
            </TableContainer>
        </Paper>
)};

export default MyLessons;
