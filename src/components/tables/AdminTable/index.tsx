import React, {FC, useEffect} from "react";
import {
    TableContainer,
    Table,
    TableRow,
    TableHead,
    TableCell,
    TableBody,
} from "@material-ui/core";
import Footer from "./Footer";
import {useApolloClient, useLazyQuery, useMutation, useQuery} from "@apollo/client";
import {DeleteButton} from "../../styled/buttons";
import {getFirstObjectKey} from "../../../utils";
import LoadingRow from "./LoadingRow";
import CRUD from "../../../models/CRUD/CRUD";

type AdminTableProps = {
    crudObject: CRUD<any>;
}

interface AnyTableDataType extends Object {
    id: number;
}

interface GottenData {
    [key: string]: Array<AnyTableDataType>;
}

interface GottenCountData {
    [key: string]: number;
}

const AdminTable: FC<AdminTableProps> = ({ crudObject }) => {
    const { columnsMask, countQuery, deleteMutation, getPaginatedQuery } = crudObject;
    const client = useApolloClient();
    const countData = useQuery<GottenCountData>(countQuery); // TODO сделать через readQuery
    const countKey: string = countData?.data ? getFirstObjectKey(countData.data) : "";
    const [rows, setRows] = React.useState<Array<AnyTableDataType>>([]);
    const [page, setPage] = React.useState<number>(0);
    const [previousLastId, setPreviousLastId] = React.useState<number>(0);
    const [rowsPerPage, setRowsPerPage] = React.useState<number>(5);
    const [deleteDataMutation] = useMutation(deleteMutation);
    const [getDataQuery, getDataQueryRes] = useLazyQuery<GottenData>(getPaginatedQuery);

    useEffect(() => {
        if (getDataQueryRes && getDataQueryRes.data) {
            const dataKey: string = getFirstObjectKey(getDataQueryRes.data);
            setRows(getDataQueryRes?.data && getDataQueryRes.data[dataKey]);
        }
    },[getDataQueryRes]);

    useEffect(() => {
        getDataQuery({variables: { limit: rowsPerPage, lastIndex: getLastRowId(0) }});
    },[rowsPerPage]);

    const changePage = (event: React.MouseEvent<HTMLButtonElement> | null, newPage: number) => {
        setPreviousLastId(!page ? 0 : (rows[0]?.id - 1));
        setPage(newPage);
        getDataQuery({variables: { limit: rowsPerPage, lastIndex: getLastRowId(newPage) }});
    };

    const getLastRowId = (newPage: number): number => {
        if (!newPage) return 0;
        if (newPage > page) return rows ? (rows[(rows.length - 1)] as AnyTableDataType)?.id | 0 : 0;
        return previousLastId;
    };

    const updateRows = () => {
        getDataQueryRes && getDataQueryRes.refetch && getDataQueryRes.refetch();
    };

    const deleteDataById = async (id: number) => {
        try {
            await deleteDataMutation({variables: { id: id }});
            setRows(rows.filter(row => row.id !== id));
            client.writeQuery({
                query: countQuery,
                data: {[countKey]: countData && countData.data && countData.data[countKey] - 1}});
        } catch (e) {
            alert("Ошибка");
        }
    };

    const changeRowsPerPage = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
    ) => {
        setRowsPerPage(parseInt(event.target.value, 10));
        setPage(0);
    };

    if (rows) {
        const emptyRows = rowsPerPage - rows.length;

        return (
            <TableContainer>
                <Table stickyHeader aria-label="custom pagination table">
                    <TableHead>
                        <TableRow key={Math.random()}>
                            {columnsMask.map(col => (
                                <TableCell key={Math.random()+1} variant={"head"} align="right">{col.label}</TableCell>
                            ))}
                            <TableCell />
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {!getDataQueryRes || getDataQueryRes.loading
                            ? (Array.from({ length: rowsPerPage })).map((el, index) => (
                               <LoadingRow key={index} />
                            ))
                            : rows.map((row) => (
                                <TableRow key={row.id}>
                                    {columnsMask.map((col, index) => (
                                        <TableCell key={`${row.id}${index}`} align="right">
                                            {row.hasOwnProperty(col.key) && ((row as any)[col.key]).toString()}
                                        </TableCell>
                                    ))}
                                    <TableCell  key={`${row.id}${Math.random()}`} align="center">
                                        <DeleteButton
                                            onClick={() => deleteDataById(row.id)}
                                            content={"Удалить запись"} />
                                    </TableCell>
                                </TableRow>
                            ))}
                        {emptyRows > 0 && (
                            <TableRow style={{ height: 54 * emptyRows }}>
                                <TableCell colSpan={6} />
                            </TableRow>
                        )}
                        </TableBody>
                    <Footer
                        getCurrentData={updateRows}
                        rowsPerPage={rowsPerPage}
                        changePage={changePage}
                        changeRowsPerPage={changeRowsPerPage}
                        page={page}
                        crudObject={crudObject}
                    />
                </Table>
            </TableContainer>
        );
    }

    return <div>Загрузка..</div>
};

export default AdminTable;
