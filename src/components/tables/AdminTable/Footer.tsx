import React, {ChangeEvent, FC, useState, MouseEvent} from "react";

import {TableFooter, TablePagination, TableRow, TableCell} from "@material-ui/core";
import TablePaginationActions from "@material-ui/core/TablePagination/TablePaginationActions";
import { useApolloClient, useMutation, useQuery} from "@apollo/client";
import {AddButton} from "../../styled/buttons";
import AddDataToAdminTable from "../../forms/AddDataToAdminTable";
import {getFirstObjectKey} from "../../../utils";
import LoadingRow from "./LoadingRow";
import CRUD from "../../../models/CRUD/CRUD";

type FooterProps = {
    crudObject: CRUD<any>;
    rowsPerPage: number;
    page: number;
    changePage: (par: MouseEvent<HTMLButtonElement> | null, pageNumber: number) => void;
    changeRowsPerPage: (par: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
    getCurrentData: () => void
};

const Footer: FC<FooterProps> = ({crudObject, rowsPerPage, page, changePage, getCurrentData, changeRowsPerPage}) => {
    const { countQuery, columnsMask, createMutation, initialValue } = crudObject;
    const client = useApolloClient();
    const getCountQueryRes = useQuery(countQuery);
    const [modalVisibility, setModalVisibility] = useState(false);
    const [createDataMutation] = useMutation(createMutation);
    const dataKey = getCountQueryRes?.data ? getFirstObjectKey(getCountQueryRes.data) : "";


    //TODO избавиться от any
    const createData = async (values: any) => {
        try {
            await createDataMutation({variables: values});
            client.writeQuery({ query: countQuery, data: {[dataKey]: Number(getCountQueryRes.data[dataKey])+1} });
            console.log("Должен произойти запрос");
            getCurrentData();
            setModalVisibility(false);
        } catch (e) {

        }
    };

    return (
        <>
            <TableFooter>
                {getCountQueryRes.loading
                    ? <LoadingRow />
                    : (
                        <TableRow>
                            <TableCell>
                                <AddButton onClick={() => setModalVisibility(true)} content="Добавить запись" />
                            </TableCell>
                            <TablePagination
                                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                                colSpan={3}
                                count={getCountQueryRes.data[dataKey]}
                                rowsPerPage={rowsPerPage}
                                page={page}
                                SelectProps={{
                                    inputProps: { 'aria-label': 'rows per page' },
                                    native: true,
                                }}
                                onChangePage={changePage}
                                onChangeRowsPerPage={changeRowsPerPage}
                                ActionsComponent={TablePaginationActions}
                            />
                        </TableRow>
                    )}
            </TableFooter>
            <AddDataToAdminTable
                submit={createData}
                columnsMask={columnsMask}
                opened={modalVisibility}
                close={() => setModalVisibility(false)}
                //TODO убрать привязку к типу SemesterType
                initialValues={initialValue}
            />
        </>
    );
};

export default Footer;
