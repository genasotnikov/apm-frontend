import React from "react";
import {
    TableRow,
    TableCell
} from "@material-ui/core";
import {Skeleton} from "@material-ui/lab";

const LoadingRow = () => {
    return (
        <TableRow>
            <TableCell colSpan={10}>
                <Skeleton width="100%" />
            </TableCell>
        </TableRow>
    );
};

export default LoadingRow;
