import React, {CSSProperties, FC} from "react";
import { Grid, GridProps } from "@material-ui/core";
import {deepPurple} from "@material-ui/core/colors";

interface StyledGridCellProps extends GridProps {
    dayIndex: number,
    isChisl: boolean,
}

const StyledGridCell: FC<StyledGridCellProps> = ({children, dayIndex, isChisl, style, ...props}) => {

    const getStyledGridCellStyles = (styles?: CSSProperties): CSSProperties => ({
        borderColor: deepPurple["A400"],
        borderStyle: "solid",
        borderWidth: 1,
        borderTopWidth: dayIndex > 0 ? 0 : 1,
        borderBottomWidth: (dayIndex === 0 && isChisl) ? 0 : 1,
        borderLeftWidth: 0,
        padding: 5,
        ...styles
    });

    return (
        <Grid {...props} style={getStyledGridCellStyles(style)}>
            {children}
        </Grid>
    );
};

export default StyledGridCell;
