import React, { FC } from "react";
import {Button, colors, ButtonBaseProps, IconButton, Grid, Tooltip} from "@material-ui/core";
import {AddCircle, Delete} from "@material-ui/icons";

interface ButtonProps extends ButtonBaseProps {
    onClick?:(params:any) => void,
    content: string
}

export const RedButton: FC<ButtonProps> = ({onClick, content}) => (
    <Button style={{color: colors.red[500]}} onClick={onClick}>{content}</Button>
);

export const GreenButton: FC<ButtonProps> = ({onClick, content, type}: ButtonProps) => (
    <Button style={{color: colors.green[500]}} onClick={onClick} type={type}>{content}</Button>
);

export const WhiteButton: FC<ButtonProps> = ({style, onClick, content, children}) => (
    <Button style={{color: "white", ...style}} onClick={onClick}>{content}{children}</Button>
);

export const BlueButton: FC<ButtonProps> = ({onClick, content}) => (
    <Button style={{color: "#3f51b5"}} onClick={onClick}>{content}</Button>
);

export const AddButton: FC<ButtonProps> = ({content, onClick}) => (
    <Tooltip title={content}>
        <Grid container direction={"column"} justify={"space-around"} xs={1} item>
            <IconButton style={{padding: 0}} onClick={onClick}>
                <AddCircle style={{color: "grey"}} />
            </IconButton>
        </Grid>
    </Tooltip>
);

export const DeleteButton: FC<ButtonProps> = ({content, onClick}) => (
    <Tooltip title={content}>
        <Grid container direction={"column"} justify={"space-around"} xs={1} item>
            <IconButton style={{padding: 0}} onClick={onClick}>
                <Delete style={{color: "grey"}} />
            </IconButton>
        </Grid>
    </Tooltip>
);
