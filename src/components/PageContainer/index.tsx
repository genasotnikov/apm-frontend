import React, {useEffect, ReactElement, FC} from "react";
import { useHistory } from "react-router";
import { Grid, Container } from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import { userDataActions } from "../../data/userData";
import NavigationMenu from "../NavigationMenu";
import ReduxState from "../../models/reduxState";
import TopPanel from "../TopPanel";

interface PageContainerProps {
    children: ReactElement | Array<ReactElement>;
}

const PageContainer: FC<PageContainerProps> = ({ children }) => {
    const auth: boolean = useSelector((state: ReduxState) => state.userData.auth);
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        dispatch(userDataActions.getUserData());
    }, []);

    useEffect(() => {
        if (!auth) history.push("login");
    },[auth]);

    return (
        <Container style={{paddingLeft:0, paddingRight: 0, alignItems: "flex-start"}} maxWidth={false}>
            <NavigationMenu />
            <Grid container spacing={3} style={{width: "87.5vw", marginLeft: "1.4vw", paddingRight: 0}} alignItems={"flex-start"}>
                <Grid item xs={12} style={{maxHeight: "5%", paddingRight: 0}}>
                    <TopPanel />
                </Grid>
                <Grid item xs={12} style={{paddingLeft: "5vw", minHeight: "90vh",/* display: "flex", flexDirection: "row"*/}}>
                    {children}
                </Grid>
            </Grid>
        </Container>
    );
};

export default PageContainer;
