import {gql} from "apollo-boost";

export const getVedomost = gql`
    query($monthIndex: Int!, $year: Int!) {
      getVedomostDays(getVedomostDaysArgs: { year: $year, monthIndex: $monthIndex, }) {
        date,
        lessonsData {
          faculty,
          dayIndex,
          groupSubgroups,
          lessonType,
          content
        }
      }
    }
`;

export const downloadVedomost = gql`
    query($monthIndex: Int!, $year: Int!) {
        uploadVedomost(getVedomostDaysArgs: { year: $year, monthIndex: $monthIndex })
    }
`;
