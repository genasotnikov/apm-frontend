import {gql} from "apollo-boost";

export const getGroupNumberViaYearAndProfile = gql`
    query ($year: Int, $profileId: Int) {
      getGroups(getGroupsArgs:{ year: $year, profileId: $profileId }) {
        id
        number
      }
    }
`;

export const getPaginatedGroup = gql`
    query($lastIndex: Int, $limit: Int) {
      getPaginatedGroups(paginatedInput: { lastIndex: $lastIndex, limit: $limit }) {
        id,
        year,
        number,
        typeOfEdu,
        formOfEdu,
        profileId
      }
    }
`;

export const createGroup = gql`
    mutation($formOfEdu: String!, $profileId: Int!, $year: Int!, $typeOfEdu: String!, $number: Int!) {
      createGroup(newGroup: { 
        formOfEdu: $formOfEdu, 
        profileId: $profileId, 
        year: $year, 
        typeOfEdu: $typeOfEdu,
        number: $number
      })
    }
`;

export const getGroupCount = gql`
    query {
        getGroupsCount
    }
`;

export const deleteGroup = gql`
    mutation($id: Int!) {
        deleteGroup(id: $id)
    }
`;
