import { gql } from 'apollo-boost';

export default gql`
    query ($email: String!, $password: String!) {
        login(LoginArgs: { email: $email, password: $password })
    }
`;