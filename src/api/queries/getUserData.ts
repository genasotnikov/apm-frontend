import { gql } from 'apollo-boost';

export default gql`
    query {
      getUserData {
        email,
        teacher {
          firstName,
          middleName,
          lastName,
          hasSchedule,
          department {
            name,
            director
          }
        }
      }
}
`;