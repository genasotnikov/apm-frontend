import {gql} from "apollo-boost";

export const getCurrentSemesterQuery = gql`
query {
    getCurrentSemester {
        year,
            isFirstSemester
    }
}`;

export const getCountOfSemester = gql`
query {
    getCountOfSemester
}`;

export const getPaginatedSemester = gql`
    query($lastIndex: Int, $limit: Int) {
      getPaginatedSemester(paginatedInput: {lastIndex: $lastIndex, limit: $limit}) {
        isFirstSemester,
        year,
         id
      }
}
`;

export const createSemester = gql`
    mutation($year: Int!, $isFirstSemester: Boolean!) {
      createSemester(SemesterInput: { year: $year, isFirstSemester: $isFirstSemester })
    }
`;

export const deleteSemester = gql`
    mutation($id: Int!) {
      deleteSemester(id: $id)
    }
`;
