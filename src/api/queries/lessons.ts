import { gql } from 'apollo-boost';

export const addLesson = gql`
    mutation($typeId: Int!, $content: String! ) {
      addLesson(AddLessonsArgs: { typeId: $typeId, content: $content }) {
        type {
          id,
          time,
          name
        },
        content,
        id
      }
    }
`;

export const lessons = gql`
    query {
      lessons {
        id,
        content,
        type {
          name
          id
          time
        }
      }
    }
`;