import getUserDataImport from "./getUserData";
import loginImport from "./login";
import { getLessonTypes as lessonTypesImport, createLessonType as createLessonTypeImport } from "./lessonTypes";
import { lessons as lessonsImport } from "./lessons";
import { getProfilesOfFaculty as getProfilesOfFacultyImport } from "./profiles";
import { getFaculties as getFacultiesImport } from "./faculties";
import { getGroupNumberViaYearAndProfile as getGroupNumberViaYearAndProfileImport } from "./groups";
import {
    getSubgroupsViaGroupId as getSubgroupsViaGroupIdImport,
    sendDaySchedule as sendDayScheduleImport,
    getDaySchedule as getDayScheduleImport} from "./subgroups";
import { getVedomost as getVedomostImport } from "./vedomost";

export const getUserData = getUserDataImport;
export const login = loginImport;

export const lessonTypes = lessonTypesImport;
export const createLessonType = createLessonTypeImport;

export const lessons = lessonsImport;

export const getProfilesOfFaculty = getProfilesOfFacultyImport;

export const getFaculties = getFacultiesImport;

export const getGroupNumberViaYearAndProfile = getGroupNumberViaYearAndProfileImport;

export const getSubgroupsViaGroupId = getSubgroupsViaGroupIdImport;

export const sendDaySchedule = sendDayScheduleImport;

export const getVedomost = getVedomostImport;
export const getDaySchedule = getDayScheduleImport;
