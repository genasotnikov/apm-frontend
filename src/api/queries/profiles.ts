import {gql} from "apollo-boost";

export const getProfilesOfFaculty = gql`
    query($facultyId: Int!) {
      getProfilesOfFaculty(getProfilesOfFacultyAgrs: { facultyId: $facultyId }) {
        id,
        name,
      }
    }
`;