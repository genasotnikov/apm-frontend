import {gql} from "apollo-boost";

export const getSubgroupsViaGroupId = gql`
    query($groupId: Int!) {
      getSubgroups(getSubgroupsArgs:{ groupId: $groupId }) {
        id
        number
      }
    }
`;

export const getSubgroupsByIds = gql`
    query($ids: [Int!]!) {
      getSubgroupsByIds(getSubgroupsByIdsArgs: { ids: $ids }) {
        id
        number
        group {
          year
          profile {
            id,
            shortness
          }
        }
      }
}
`;

export const sendDaySchedule = gql`
   mutation($lessons: [AddSubgroupLessonTypeArgs!]!) {
      addArraySubgroupLesson(AddArraySubgroupLessonTypeArgs: { lessons: $lessons } )
    }
`;

export const getDaySchedule = gql`
    query($weekIndex: Int!, $semesterId: Int! ) {
      getSubgroupLessons(GetSubgroupLessonsArgs: {arg: { 
        weekIndex: $weekIndex, semesterId: $semesterId }}) {
        id,
        subgroup {
          id,
          number,
          group {
            year,
            number,
          }
        }
        lessonInSchedule {
          id,
          dayIndex, 
          isChisl,
          lesson {
            id
          }
        }
      }
    }
`;

export const deleteLessonFromSchedule = gql`
    mutation($lessonInScheduleId: Int!) {
    deleteSubgroupLessonsByLessonImScheduleId(LessonInScheduleId: $lessonInScheduleId)
    }
`;
