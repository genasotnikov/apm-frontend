import {gql} from "apollo-boost";

export const getFaculties = gql`
    query {
      getFaculties {
        id,
        shortness
      }
    }
`;