import { gql } from 'apollo-boost';

export const getLessonTypes = gql`
    query {
      lessonTypes {
        id,
        name,
        time
      }
    }
`;

export const createLessonType = gql`
    mutation($name: String!, $time: Float!) {
      addLessonType(AddLessonTypeArgs: { name: $name, time: $time }) {
        id, name, time
      }
   }`;
