import {createAction} from "@reduxjs/toolkit";
import {DayData, TypeOfLoad} from "../../models/reduxState/VedomostState";
import ReduxState from "../../models/reduxState";
import client from "../../api";
import {getVedomost as getVedomostQuery} from "../../api/queries";

const getTypesOfLoad = (arg1: Array<TypeOfLoad>, arg2: Array<DayData>): Array<TypeOfLoad> => {
    const res = arg1.slice(0);
    arg2.forEach((el) => {
        el.lessonsData.forEach(lesson => {
            const foundIndex = res.findIndex(el => el.content === lesson.lessonType);
            if (foundIndex !== -1) {
                res[foundIndex] = { ...res[foundIndex], time: res[foundIndex].time + 2};
            }
            else res.push({ time: 2, content: lesson.lessonType });
        })
    });
    return res;
};

export const setVedomostLoading = createAction<void>("SET_VEDOMOST_LOADING");

export const setVedomost = createAction<Array<DayData>>("SET_VEDOMOST");

export const setVedomostError = createAction("SET_VEDOMOST_ERROR");

export const setTypesOfLoad = createAction<Array<TypeOfLoad>>("SET_TYPES_OF_LOAD");
export const resetTypesOfLoad = createAction<void>("RESET_TYPES_OF_LOAD");

export const getVedomost = (monthIndex:number) => async (dispatch: (action: Object) => null, getState: () => ReduxState) => {
    const state: any = getState() as ReduxState;
    const jwt = state.userData.jwt;
    try {
        const { data, loading, errors } = await client.query({
            query: getVedomostQuery,
            variables: { monthIndex: monthIndex, year: 2020 }, //TODO год не должен быть константой
            context: { headers: {"Authorization": jwt} }});
        if (errors) alert(errors);
        if (loading) dispatch(setVedomostLoading());
        if (data) {
            const vedomostMonthData: Array<DayData> = data.getVedomostDays;
            dispatch(resetTypesOfLoad());
            const typesOfLoad: Array<TypeOfLoad> = getState().vedomost.typesOfLoad;
            dispatch(setTypesOfLoad(getTypesOfLoad(typesOfLoad, vedomostMonthData)));
            dispatch(setVedomost(vedomostMonthData));
        }
    }
    catch (e) {
        alert("Данные введены некорректно");
        return;
    }
};
