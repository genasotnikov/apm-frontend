import { PayloadAction} from "@reduxjs/toolkit";
import VedomostState from "../../models/reduxState/VedomostState";
import { setVedomostLoading, setVedomost, getVedomost, setTypesOfLoad, resetTypesOfLoad } from "./actions";

const initialTypesOfLoad = [
    { content:"Лекции", time: 0 }, { content: "Лабораторные работы", time: 0 }, { content:"Курсовая работа", time: 0 },
    { content: "Консультации", time: 0 }, { content: "Зачеты", time: 0 }, { content:"Экзамены", time: 0 },
    { content: "Работа с аспирантами", time : 0 }, { content: "доп.час.лаб.оч.", time: 0}];

const initialState = {
    vedomost: {},
    loading: true,
    typesOfLoad: initialTypesOfLoad
} as VedomostState;

export const vedomostReducer = (state=initialState, action: PayloadAction) => {
    switch (action.type) {
        case setVedomostLoading.type: {
            return {
                ...state,
                loading: true,
            }
        }
        case setVedomost.type: {
            return {
                ...state,
                vedomost: action.payload,
                loading: false,
            }
        }
        case setTypesOfLoad.type: {
            return {
                ...state,
                typesOfLoad: action.payload
            }
        }
        case resetTypesOfLoad.type: {
            return {
                ...state,
                typesOfLoad: initialTypesOfLoad
            }
        }
        default: return state;
    }
};

export const vedomostActions = {
    getVedomost
};
