import {createAction, createSlice, PayloadAction} from "@reduxjs/toolkit";
import client from "../api";
import { getUserData as getUserDataQuery, login as loginQuery } from "../api/queries";
import UserDataState, {Teacher} from "../models/reduxState/userData";
import ReduxState from "../models/reduxState";

const setAuthByJWT = createAction<string>("SET_AUTH_BY_JWT");
const setTeacherData = createAction<Teacher>("SET_TEACHER_DATA");
const setLoginLoading = createAction("SET_LOGIN_LOADING");
const setError = createAction<string>("SET_USER_ERROR");

const logOut = createAction("LOGOUT");

const getUserData = () => async (dispatch: (action: Object) => null, getState: () => ReduxState) => {
    const jwt = getState().userData.jwt;
    const { data, loading, errors } = await client.query({
        query: getUserDataQuery,
        context: { headers: {"Authorization": jwt} }
    });
    if (loading) dispatch(setLoginLoading());
    if (errors) {
        dispatch(setError(errors.toString()));
        return;
    }
    if (data) {
        dispatch(setTeacherData(data));
    }
};

const login = (values: Object) => async (dispatch: (action: Object) => null) => {
    const { data, loading, errors } = await client.query({ query: loginQuery, variables: { ...values } });
    if (loading) dispatch(setLoginLoading());
    if (errors) {
        dispatch(setError(errors.toString()));
        return;
    }
    if (data) {
        dispatch(userDataActions.setAuthByJWT(data.login));
    }
};

const initialState : UserDataState = {
    auth: false,
    loading: false,
    jwt: null as String | null,
    teacher: null,
    error: "",
};

const userData = createSlice({
    name: "userData",
    initialState: initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(setAuthByJWT, (state, action: PayloadAction<string>) => {
            return {
                ...state,
                auth: true,
                jwt: action.payload
            }
        });
        builder.addCase(setError, (state, action: PayloadAction<string>) => {
            return {
                ...state,
                error: action.payload
            }
        });
        builder.addCase(logOut, () => {
            return initialState;
        });
        builder.addCase(setLoginLoading, (state) => {
            return {
                ...state,
                loading: true
            }
        });
        builder.addCase(setTeacherData, (state, action: PayloadAction<any>) => {
            return {
                ...state,
                teacher: action.payload.getUserData.teacher as Teacher,
                loading: false
            }
        })
    }
});

export const userDataReducer = userData.reducer;
export const userDataActions = {
    setAuthByJWT,
    logOut,
    login,
    getUserData
};
