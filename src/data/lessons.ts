import {createSlice, PayloadAction, createAction} from "@reduxjs/toolkit";
import {Lesson} from "../models/shedule";
import { lessons as lessonQuery, } from "../api/queries";
import LessonsState from "../models/reduxState/lessons";
import {getDataLoadAsyncAction, mutateDataAsyncAction} from "./utils";
import {addLesson} from "../api/queries/lessons";


const setLessons = createAction<Array<Lesson>>("SET_LESSONS");
const setLessonsLoading = createAction("SET_LESSONS_LOADING");
const setError = createAction<string>("SET_LESSON_ERROR");

const getLessons = getDataLoadAsyncAction<Lesson>({
    dataKey: "lessons",
    loadDataQuery: lessonQuery,
    loadingAction: setLessonsLoading,
    setDataAction: setLessons,
    setErrorAction: setError
});


const createLesson = mutateDataAsyncAction({
    dataKey: "addLessonType",
    setErrorAction: setError,
    mutation: addLesson,
    setDataAction: setLessons,
    reducerName: "lessons",
});


const initialState : LessonsState = {
    lessons: [],
    error: "",
    loading: false
};

const lessons = createSlice({
    name: "lessons",
    initialState: initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(setLessons, (state, action: PayloadAction<Array<Lesson>>) => ({
            ...state,
            loading: false,
            lessons: action.payload
        }));
        builder.addCase(setError, (state, action: PayloadAction<string>) => ({
                ...state,
                loading: false,
                error: action.payload
            })
        );
        builder.addCase(setLessonsLoading, (state) => {
            return {
                ...state,
                loading: true
            }
        });
    }
});

export const lessonsDataReducer = lessons.reducer;
export const lessonsActions = {
    getLessons,
    createLesson
};