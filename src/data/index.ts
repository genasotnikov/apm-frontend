import { createStore, combineReducers, applyMiddleware } from "redux";
import { persistStore, persistReducer } from 'redux-persist';
import thunk from "redux-thunk";
import storage from 'redux-persist/lib/storage';
import { userDataReducer } from "./userData";
import { lessonsDataReducer } from "./lessons";
import {lessonTypesReducer} from "./lessonTypes";
import {daySchedule as dayScheduleReducer} from "./daySchedule";
import { vedomostReducer } from "./vedomost";


const persistConfig = {
    key: 'root',
    storage,
};

const reducer = combineReducers({
    userData: userDataReducer,
    lessons: lessonsDataReducer,
    lessonTypes: lessonTypesReducer,
    daySchedule: dayScheduleReducer,
    vedomost: vedomostReducer
});

const persistedReducer = persistReducer(persistConfig, reducer);



export const store = createStore(persistedReducer, {} ,applyMiddleware(thunk));
export const persistor = persistStore(store);
