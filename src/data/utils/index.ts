import {DocumentNode} from "graphql";
import ReduxState, {ReduxStateKeys} from "../../models/reduxState";
import client from "../../api";

type DataLoadAsyncActionArg <T> = {
    loadDataQuery: DocumentNode,
    loadingAction: () => Object,
    setErrorAction: (arg:string) => Object,
    setDataAction: (arg: Array<T>) => Object,
    dataKey: string
};

type MutateDataAsyncActionArg <T> = {
    mutation: DocumentNode,
    setErrorAction: (arg:string) => Object,
    setDataAction: (arg: Array<T>) => Object,
    dataKey: string,
    reducerName: ReduxStateKeys
};

export const getDataLoadAsyncAction: <T>(arg:DataLoadAsyncActionArg<T>) => Function =
    <T>({loadDataQuery, loadingAction, setErrorAction, setDataAction, dataKey}: DataLoadAsyncActionArg<T>) =>
        () => async (dispatch: (action: Object) => null, getState: () => ReduxState) => {
            const jwt = getState().userData.jwt;
            try {
                const {data, loading} = await client.query({
                    query: loadDataQuery,
                    context: {headers: {"Authorization": jwt}}
                });
                if (loading) dispatch(loadingAction());
                if (data) dispatch(setDataAction(data[dataKey] as Array<T>));
            } catch (e) {
                alert(e);
                dispatch(setErrorAction(e.toString()));
                return;
            }
    };

export const mutateDataAsyncAction: <T>(arg:MutateDataAsyncActionArg<T>) => Function =
    <T>({mutation, setErrorAction, setDataAction, dataKey, reducerName}: MutateDataAsyncActionArg<T>) =>
        (variables: T) => async (dispatch: (action: Object) => null, getState: () => ReduxState) => {
            const state:any = getState() as ReduxState;
            const jwt = state.userData.jwt;
            try {
                const { data } = await client.mutate({
                    mutation: mutation,
                    variables: variables,
                    context: { headers: {"Authorization": jwt} }});
                if (data[dataKey]) {
                    dispatch(setDataAction([...state[reducerName][reducerName], data[dataKey]]));
                }
            } catch (e) {
                dispatch(setErrorAction(e.toString()));
                alert(e);
                return;
            }
        };