import {createSlice, PayloadAction, createAction} from "@reduxjs/toolkit";
import LessonTypesState from "../models/reduxState/lessonTypes";
import {LessonType} from "../models/shedule";
import { lessonTypes as lessonTypesQuery, createLessonType as createLessonTypeQuery } from "../api/queries";
import {getDataLoadAsyncAction, mutateDataAsyncAction} from "./utils";

const setLessonTypes = createAction<Array<LessonType>>("SET_LESSON_TYPES");
const setLessonTypesLoading = createAction("SET_LESSON_TYPES_LOADING");
const setError = createAction<string>("SET_USER_ERROR");

const getLessonTypes = getDataLoadAsyncAction<LessonType>({
    dataKey: "lessonTypes",
    loadDataQuery: lessonTypesQuery,
    setErrorAction: setError,
    setDataAction: setLessonTypes,
    loadingAction: setLessonTypesLoading
});

const createLessonType = mutateDataAsyncAction<LessonType>({
    dataKey: "addLessonType",
    setErrorAction: setError,
    mutation: createLessonTypeQuery,
    setDataAction: setLessonTypes,
    reducerName: "lessonTypes",
});

const initialState : LessonTypesState = {
    loading: false,
    lessonTypes: [],
    error: "",
};

const lessonTypes = createSlice({
    name: "lessonTypes",
    initialState: initialState,
    reducers: {},
    extraReducers: builder => {
        builder.addCase(setLessonTypes, (state, action: PayloadAction<Array<LessonType>>) => ({
                ...state,
                loading: false,
                lessonTypes: action.payload
        }));
        builder.addCase(setError, (state, action: PayloadAction<string>) => ({
                ...state,
                loading: false,
                error: action.payload
            })
        );
        builder.addCase(setLessonTypesLoading, (state) => {
            return {
                ...state,
                loading: true
            }
        });
    }
});

export const lessonTypesReducer = lessonTypes.reducer;
export const lessonTypeActions = {
    getLessonTypes,
    createLessonType
};