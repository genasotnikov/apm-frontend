import {createAction} from "@reduxjs/toolkit";
import {
    addGroupPayload,
    addLessonPayload,
    hourValue, removeLessonFromSchedulePayload,
} from "../../models/reduxState/daySchedule";
import ReduxState from "../../models/reduxState";
import {SubgroupLessonImport} from "../../models/shedule";
import client from "../../api";
import { sendDaySchedule as sendDayScheduleMutation, getDaySchedule as getDayScheduleQuery } from "../../api/queries";
import SubgroupLesson from "../../models/Models/SubgroupLesson";
import Subgroup from "../../models/Models/Subgroup";
import {deleteLessonFromSchedule as deleteLessonFromScheduleMutation} from "../../api/queries/subgroups";


const resetData = createAction("RESET_DAY_SCHEDULE");

const addGroupToHourSchedule = createAction<addGroupPayload>("ADD_GROUP_TO_HOUR_SCHEDULE");
const addLessonToHourSchedule = createAction<addLessonPayload>("ADD_LESSON_TO_HOUR_SCHEDULE");
const setWeekIndex = createAction<number>("SET_WEEK_INDEX");
const setDaySchedule = createAction<Array<Array<hourValue>>>("SET_DAY_SCHEDULE");
const setDayScheduleLoading = createAction<boolean>("SET_DAY_SCHEDULE_LOADING");
const removeLessonFromSchedule = createAction<removeLessonFromSchedulePayload>("REMOVE_LESSON_FROM_SCHEDULE");

const convertToSubgroupLessonImport = (dataInForm: Array<Array<hourValue>>, weekIndex: number): Array<SubgroupLessonImport> => {
    const res1: Array<hourValue> = dataInForm.reduce((accumulator, hourSch) => [...accumulator, hourSch[0], hourSch[1]] ,[]);
    const res2: Array<SubgroupLessonImport> = res1.reduce((accumulator:Array<SubgroupLessonImport>, lesson, index) => {
        if (lesson.groups && lesson.groups.length && Number(lesson.lessonId)) {
            const newRes:Array<SubgroupLessonImport> = lesson.groups.map(el => ({
                isChisl: !index,
                weekIndex: weekIndex,
                dayIndex: Math.trunc(index / 2),
                subgroupId: el,
                lessonId: Number(lesson.lessonId)
            }));
            return [...accumulator, ...newRes];
        }
        return [...accumulator];
    }, []);
    return res2;
};

const getChislInd = (isChisl: boolean) => {
    return isChisl ? 0 : 1;
};

const subgroupLessonsToDataInForm = (sgLessons: Array<Partial<SubgroupLesson>>): Array<Array<hourValue>> => {
    let res: Array<Array<hourValue>> = [];
    sgLessons.forEach(sgLesson => {
        if (sgLesson?.lessonInSchedule?.dayIndex === undefined || sgLesson?.lessonInSchedule?.isChisl === undefined) {
            console.error("got wrong data (getDayScheduleQuery)");
            return;
        }
        const lessonInSchedule = sgLesson.lessonInSchedule;
        let hourLessonData = res[lessonInSchedule.dayIndex];
        const chislInd = getChislInd(lessonInSchedule.isChisl);
        if (hourLessonData && hourLessonData[chislInd]) {
            if (!Array.isArray(hourLessonData[chislInd].groups))
                hourLessonData[chislInd].groups = [];
            (hourLessonData[chislInd].groups as Array<number>)
                .push(Number((sgLesson.subgroup as Subgroup).id));
            hourLessonData[chislInd].lessonInScheduleId = Number(lessonInSchedule.id);
            hourLessonData[chislInd].lessonId = Number(lessonInSchedule.lesson.id);
        } else {
            let el: hourValue = {};
            el.lessonInScheduleId = lessonInSchedule.id;
            el.lessonId = lessonInSchedule.lesson?.id;
            el.groups = [Number(sgLesson?.subgroup?.id) || -1] ;
            if (hourLessonData) hourLessonData[chislInd] = el;
            else {
                hourLessonData = [{}, {}];
                hourLessonData[chislInd] = el;
                res[lessonInSchedule.dayIndex] = hourLessonData;
            }
        }
    });
    return res;
};

const getDaySchedule = (weekIndex: number, semesterId: number) =>
    async (dispatch: (action: Object) => null, getState: () => ReduxState) => {
        dispatch(resetData());
        const state: any = getState() as ReduxState;
        // сделать запрос (SubgroupLesson)
        const jwt = state.userData.jwt;
        try {
            dispatch(setDayScheduleLoading(true));
            const { data, error } = await client.query({
                query: getDayScheduleQuery,
                // TODO изменить для получения расписания знаменателя
                variables: { weekIndex: weekIndex, semesterId: semesterId },
                context: { headers: {"Authorization": jwt} }});
            if (data["getSubgroupLessons"]) {
                const newFormData = subgroupLessonsToDataInForm(data["getSubgroupLessons"] as Array<Partial<SubgroupLesson>>);
                dispatch(setDaySchedule(newFormData));
                dispatch(setDayScheduleLoading(false));
            }
            if (error) dispatch(setDayScheduleLoading(false));
        } catch (e) {
            dispatch(setDayScheduleLoading(false));
            console.error("Ошибка запроса getDaySchedule error:", e);
            return;
        }
    };

const sendDaySchedule = () => async (dispatch: (action: Object) => null, getState: () => ReduxState) => {
    const state:any = getState() as ReduxState;
    const dataInForm = state.daySchedule.formData.hourSchedules;
    const weekIndex = state.daySchedule.weekIndex;
    const dataToSend:Array<SubgroupLessonImport> = convertToSubgroupLessonImport(dataInForm, weekIndex);
    const jwt = state.userData.jwt;
    try {
        const { data } = await client.mutate({
            mutation: sendDayScheduleMutation,
            variables: { lessons: dataToSend },
            context: { headers: {"Authorization": jwt} }});
        if (data["addArraySubgroupLesson"]) {
            alert("Расписание на день успешно сохранено!");
        }
    } catch (e) {
        alert("Данные введены некорректно");
        return;
    }
};

const deleteLessonInSchedule = (dayIndex: number, chislInd: number) =>
    async (dispatch: (action: Object) => null, getState: () => ReduxState) => {
    const state: any = getState() as ReduxState;
    const jwt = state.userData.jwt;
    const lessonInScheduleId = state.daySchedule.formData?.hourSchedules[dayIndex][chislInd]?.lessonInScheduleId;
    if (lessonInScheduleId) {
        try {
            const { data } = await client.mutate({
                mutation: deleteLessonFromScheduleMutation,
                variables: { lessonInScheduleId: Number(lessonInScheduleId) },
                context: { headers: {"Authorization": jwt} }});
            if (data["addArraySubgroupLesson"]) {
                console.log("Удаление прошло успешно");
            }
            dispatch(removeLessonFromSchedule({dayIndex, chislInd}));
        } catch (e) {
            alert("Данные введены некорректно");
            return;
        }
    }
};

export default {
    addGroupToHourSchedule,
    addLessonToHourSchedule,
    setDayScheduleLoading,
    resetData,
    setWeekIndex,
    sendDaySchedule,
    getDaySchedule,
    setDaySchedule,
    removeLessonFromSchedule,
    deleteLessonInSchedule
};
