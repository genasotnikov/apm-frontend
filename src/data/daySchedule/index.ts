import dayScheduleActions from "./actions";

import {
    dayScheduleState,
    hourValue,
} from "../../models/reduxState/daySchedule";

const initialHourSchedules = [[{}, {}], [{},{}], [{},{}], [{},{}], [{},{}], [{},{}]];


const initialState: dayScheduleState = {
    formData: { hourSchedules: initialHourSchedules },
    weekIndex: 0,
    dayScheduleLoading: false,
};

export const daySchedule = (state: dayScheduleState = initialState, action: any) => {
    switch (action.type) {
        case dayScheduleActions.resetData.type:
            return {
                ...state,
                formData: { hourSchedules: [[{}, {}], [{},{}], [{},{}], [{},{}], [{},{}], [{},{}]] }
            };
        case dayScheduleActions.addGroupToHourSchedule.type: {
            const oldHourSch:hourValue = state.formData.hourSchedules[action.payload.dayIndex][action.payload.chislInd];
            const newHourSch:hourValue = (!oldHourSch.groups || !oldHourSch.groups.length)
                ? {...oldHourSch, groups: [action.payload.newValue]} as hourValue
                : { ...oldHourSch, groups: [...oldHourSch.groups, action.payload.newValue] };
            let newHouhrSchs:Array<Array<hourValue>>  = [...state.formData.hourSchedules];
            newHouhrSchs[action.payload.dayIndex][action.payload.chislInd] = newHourSch;
            return {
                ...state,
                formData: { hourSchedules: newHouhrSchs }
            };
        }
        case dayScheduleActions.addLessonToHourSchedule.type: {
            const oldHourSchedules = Array.isArray(state.formData?.hourSchedules) ? [...state.formData.hourSchedules] : [];
            const chislAndZnam = oldHourSchedules[action.payload.dayIndex] || new Array<hourValue>();
            const oldHourSch:hourValue = chislAndZnam[action.payload.chislInd] || {};
            const newHourSch:hourValue = { ...oldHourSch, lessonId: action.payload.newValue };
            let newHouhrSchs:Array<Array<hourValue>>  = [...state.formData.hourSchedules];
            newHouhrSchs[action.payload.dayIndex] = chislAndZnam;
            newHouhrSchs[action.payload.dayIndex][action.payload.chislInd] = newHourSch;
            return {
                ...state,
                formData: { hourSchedules: newHouhrSchs }
            };
        }
        case dayScheduleActions.setWeekIndex.type: {
            return {
                ...state,
                weekIndex: action.payload
            }
        }
        case dayScheduleActions.removeLessonFromSchedule.type: {
            return {
                ...state,
                formData: {
                    hourSchedules: {
                        ...state.formData.hourSchedules,
                        [action.payload.dayIndex]: {
                            ...state.formData.hourSchedules[action.payload.dayIndex],
                            [action.payload.chislInd]: {}
                        }
                    }}
            }
        }
        case dayScheduleActions.setDaySchedule.type: {
            const newHourSchedules = [...initialHourSchedules];
            const gottenData: Array<Array<hourValue>> = action.payload;
            for (let key in newHourSchedules) {
                if (gottenData[key]) newHourSchedules[key] = gottenData[key];
            }
            return {
                ...state,
                formData: {
                    hourSchedules: newHourSchedules
                }
            }
        }
        case dayScheduleActions.setDayScheduleLoading.type: {
            return {
                ...state,
                dayScheduleLoading: action.payload
            }
        }
        default: return {...state};
    }
};

