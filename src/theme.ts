import {createMuiTheme, colors} from "@material-ui/core";
import {deepOrange} from "@material-ui/core/colors";


const palette = {
        primary: {
            main: colors.indigo["500"]
        },
        secondary: {
            main: colors.green.A400
        },
        error: {
            main: colors.green.A400
        }
    };
export default createMuiTheme({
    overrides: {
        MuiTableCell: {
            stickyHeader: {
                backgroundColor: deepOrange[500],
                color: "white"
            }
        },
        MuiTable: {
            stickyHeader: {
                border: `1px solid ${deepOrange[500]}`
            }
        },
        MuiTableContainer: {
          root: {
              maxHeight: 407,
          },
        },
        MuiToolbar: {
            root: {
                paddingTop: 5,
                paddingBottom: 5,
            }
        },
        MuiDrawer: {
          root: {
              width: "10vw",
              color: "white",
              backgroundColor: colors.indigo["500"]
          }
        },
        MuiTypography: {
            root: {
                textAlign: "center"
            }
        },
        MuiCard: {
            root: {
                color: "black",
                backgroundColor: "white",
                padding: 10,
            }
        },
        MuiPaper: {
            root: {
                backgroundColor: colors.indigo["500"],
                color: "white"
            },
        },
        MuiTextField: {
          root: {
              width: "100%"
          }
        },
        MuiInput: {
            root: {
                width: "100%"
            }
        },
        MuiButton: {
            root: {
                width: "100%"
            }
        },
        MuiDialogContent: {
            root: {
                backgroundColor: "white"
            },
        },
        MuiDialogActions: {
            root: {
                backgroundColor: "white"
            }
        },
        MuiContainer: {
            root: {
                height: window.innerHeight,
                display: "flex",
                maxWidth: "unset"
            }
        },
    },
    palette

});
