import React from "react";
import {Router, Switch, Route} from "react-router";
import { Provider } from "react-redux";
import { MuiThemeProvider } from "@material-ui/core"
import { ApolloProvider } from '@apollo/client';
import { createBrowserHistory } from "history";
import {Home, Login, Schedule, Vedomosti, AddWork, DBMS} from "./components/screens";
import Registration from "./components/forms/Registration";
import client from "./api";
import theme from "./theme";
import { store, persistor } from "./data";
import { PersistGate } from "redux-persist/integration/react";

const App = () => {
  const history = createBrowserHistory();
  return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <ApolloProvider client={client}>
            <MuiThemeProvider theme={theme}>
              <Router history={history}>
                <Switch>
                  <Route path="/registration">
                    <Registration />
                  </Route>
                  <Route path="/login">
                    <Login />
                  </Route>
                  <Route path="/schedule">
                    <Schedule />
                  </Route>
                  <Route path="/vedomosti">
                    <Vedomosti />
                  </Route>
                  <Route path="/addWork">
                    <AddWork />
                  </Route>
                  <Route path="/myLessons">
                    <Home />
                  </Route>
                  <Route path="/DBMS">
                    <DBMS />
                  </Route>
                  <Route path="/">
                    <Home />
                  </Route>
                </Switch>
              </Router>
            </MuiThemeProvider>
          </ApolloProvider>
        </PersistGate>
      </Provider>
  );
};

export default App;
